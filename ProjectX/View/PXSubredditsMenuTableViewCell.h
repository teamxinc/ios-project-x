//
//  PXSubredditsMenuTableViewCell.h
//  Probe
//
//  Created by David Attaie on 22/06/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PXSubredditsMenuTableViewCell : UITableViewCell

-(void)setupCellWithName:(NSString *)name;

@end
