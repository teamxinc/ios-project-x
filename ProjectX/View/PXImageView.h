//
//  PXImageView.h
//  Probe
//
//  Created by David Attaie on 14/03/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PXImageView : UIView

-(void)setImage:(UIImage *)image;

@end
