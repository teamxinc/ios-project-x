//
//  PXCardCellCell.m
//  Probe
//
//  Created by David Attaie on 05/05/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "PXCardCell.h"


@implementation PXCardCell

-(void)cleanStrongPointers
{
	[_mainLabel removeFromSuperview];
	_mainLabel = nil;
	[_topView removeFromSuperview];
	_topView = nil;
	[_outlineLabel removeFromSuperview];
	_outlineLabel = nil;
	[_containerView removeFromSuperview];
	_containerView = nil;
}

-(void)dealloc
{
	[self cleanStrongPointers];
}

-(void)setupBackground
{
	if (!self.outlineLabel) {
		self.outlineLabel = [[UIImageView alloc] initWithFrame:CGRectMake(kStartingPositionX,
																		  kStartingPositionY,
																		  self.frame.size.width - 2 * kStartingPositionX,
																		  self.frame.size.height - 2 * kStartingPositionY)];
		
		UIImage* image = [UIImage imageNamed:@"card"];
		UIEdgeInsets insets = UIEdgeInsetsMake(4, 4, 5, 5);
		image = [image resizableImageWithCapInsets:insets];
		[self.outlineLabel setImage:image];
		[self addSubview:self.outlineLabel];
		
		UIView *whiteView = [[UIView alloc] initWithFrame:CGRectMake(kStartingPositionX + kWidthOfBorder,
																	 kStartingPositionY + kWidthOfBorder,
																	 self.frame.size.width - 2 * (kStartingPositionX + kWidthOfBorder),
																	 self.frame.size.height - 2 * (kStartingPositionY + kWidthOfBorder))];
		[whiteView setBackgroundColor:[UIColor clearColor]];
		[self addSubview:whiteView];
		
		self.containerView = whiteView;
		
		[self.outlineLabel setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth];
		[self.containerView setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth];
	}
}

-(void)setupTopSection
{
	if (!self.topView) {
		self.topView = [[UIView alloc] initWithFrame:CGRectMake(kStartingPositionX + kWidthOfBorder,
																kStartingPositionY + kWidthOfBorder,
																self.frame.size.width - 2  * (kStartingPositionX + kWidthOfBorder),
																kHeightOfTop)];
		[self.topView setBackgroundColor:[UIColor greenColor]];
		[self addSubview:self.topView];
	}
}

-(void)setupMainLabel
{
	if (!self.mainLabel) {
		float start = kStartingPositionX + kWidthOfBorder + 10;
		self.mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(start,
																   CGRectGetHeight(self.topView.frame) + 38,
																   self.frame.size.width - (2 * start),
																   35)];
		[self.mainLabel setNumberOfLines:2];
		[self.mainLabel setFont:[UIFont fontWithName:kMainFont size:17]];
		[self.mainLabel setTextColor:[PXProbeHelpers colorWithRed:62 green:62 blue:62]];
		[self addSubview:self.mainLabel];
	}
}

@end
