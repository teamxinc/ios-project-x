//
//  PXSubredditsMenuTableViewCell.m
//  Probe
//
//  Created by David Attaie on 22/06/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "PXSubredditsMenuTableViewCell.h"

@implementation PXSubredditsMenuTableViewCell

-(void)setupCellWithName:(NSString *)name
{
	UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(2, 2, CGRectGetWidth(self.bounds) - 4, CGRectGetHeight(self.bounds) - 2)];
	[backgroundView setBackgroundColor:[UIColor colorWithRed:98.0/255.0f green:195.0f/255.0f blue:180.0f/255.0f alpha:1.0f]];
	[self addSubview:backgroundView];
	
	UIImageView *close = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"close"]];
	[close setFrame:CGRectMake(0, 0, CGRectGetHeight(backgroundView.bounds), CGRectGetHeight(backgroundView.bounds))];
	[backgroundView addSubview:close];
	
	
	float labelStart = CGRectGetMaxX(close.frame) + 20;
	UILabel *subredditLabel = [[UILabel alloc] initWithFrame:CGRectMake(labelStart,
																		0,
																		CGRectGetWidth(backgroundView.bounds) - labelStart,
																		CGRectGetHeight(backgroundView.bounds))];
	[subredditLabel setFont:[UIFont fontWithName:kMainFont size:16]];
	[subredditLabel setTextColor:[UIColor whiteColor]];
	[subredditLabel setText:name];
	[backgroundView addSubview:subredditLabel];
	
}

@end
