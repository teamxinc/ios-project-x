//
//  PXCardCellCell.h
//  Probe
//
//  Created by David Attaie on 05/05/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "PXProbeHelpers.h"

//default type sizing
#define kPhotoCellSize		200.0f
#define kGifCellSize		200.0f
#define kTextCellSize		125.0f

//default positions/frames
#define kStartingPositionX	4.0f
#define kStartingPositionY	4.0f
#define kWidthOfBorder		4.0f
#define kHeightOfTop		30.0f
#define kVerticalSpace		4.0f

@interface PXCardCell : UITableViewCell

@property (strong, nonatomic) UIView *topView;
@property (strong, nonatomic) UIImageView *outlineLabel;
@property (strong, nonatomic) UIView *containerView;
@property (strong, nonatomic) UILabel *mainLabel;


//inheritance based setup function
-(void)setupTopSection;
-(void)setupBackground;
-(void)setupMainLabel;
-(void)cleanStrongPointers;
@end
