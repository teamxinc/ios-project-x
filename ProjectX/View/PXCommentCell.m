//
//  PXCommentCell.m
//  Probe
//
//  Created by David Attaie on 05/05/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "PXProbeHelpers.h"
#import "PXCommentCell.h"

@interface PXCommentCell()

@property (nonatomic, strong) Comment *comment;
@property (nonatomic, weak) UILabel *headerLabel;
@property (nonatomic, weak) UIButton *expandButton;
@property (nonatomic, weak) UIImageView *parentHighlight;

@property (nonatomic, weak) UIView *topLine;
@property (nonatomic, weak) UIView *colorIndent;

@end

@implementation PXCommentCell

-(void)setupWithComment:(Comment *)comment
{
	[self setSelectionStyle:UITableViewCellSelectionStyleNone];
	[self resetProperties];
	
	[self setComment:comment];
	
	[self setupBackground];
	[self setupTopSection];
	[self setupMainLabel];
    [self setupOutline];
    [self setupButton];
	
	[self.mainLabel setText:comment.text];
	[self.mainLabel sizeToFit];
}

-(void)setupButton
{
    if ([[self.comment children] count] > 0) {
        UIButton *expandButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [expandButton.titleLabel setFont:[UIFont fontWithName:kMainFontMed size:30]];
        [expandButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [expandButton setFrame:CGRectMake(0, 0, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds))];
        [expandButton addTarget:self action:@selector(expandComments:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:expandButton];
        self.expandButton = expandButton;
    }
}

-(void)setupTopSection
{
	float start = kStartingPositionX + kWidthOfBorder + 10;
	UILabel *topLabel = [[UILabel alloc] initWithFrame:CGRectMake(start + [self insetWidth], CGRectGetMaxY(self.topView.bounds) + 5, CGRectGetWidth(self.bounds) - [self insetWidth], 30)];
	NSString *formattedTitle = [NSString stringWithFormat:@"%@ • %@", self.comment.author,
								[PXProbeHelpers calculateLastTimeForDate:self.comment.created_utc]];
	[topLabel setText:formattedTitle];
	
	[topLabel setFont:[UIFont fontWithName:kMainFont size:14]];
	[topLabel setTextColor:[UIColor colorWithWhite:0.5f alpha:1.0f]];
	self.headerLabel = topLabel;

	[self addSubview:topLabel];
}

-(void)setupBackground
{
	UIImageView *doubleOutline = [[UIImageView alloc] initWithFrame:CGRectMake(kStartingPositionX + 2 + [self insetWidth],
																			   kStartingPositionY + 2,
																			   self.frame.size.width - 2 * kStartingPositionX + 2 - [self insetWidth],
																			   self.frame.size.height - 2 * kStartingPositionY + 2)];
	
	[self.outlineLabel setFrame:CGRectMake(CGRectGetMinX(self.outlineLabel.frame) + [self insetWidth], CGRectGetMinY(self.outlineLabel.frame), CGRectGetWidth(self.outlineLabel.frame) - [self insetWidth], CGRectGetHeight(self.outlineLabel.frame))];
	
	[self.containerView setFrame:CGRectMake(CGRectGetMinX(self.containerView.frame) + [self insetWidth], CGRectGetMinY(self.containerView.frame), CGRectGetWidth(self.containerView.frame) - [self insetWidth], CGRectGetHeight(self.containerView.frame))];
}

-(void)setupOutline
{
    UIView *topBanner = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.headerLabel.frame), 0, CGRectGetWidth(self.bounds) - CGRectGetMinX(self.headerLabel.frame), 1)];
    [topBanner setBackgroundColor:[UIColor lightGrayColor]];
    [self addSubview:topBanner];
    self.topView = topBanner;
    
    UIView *insetView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.headerLabel.frame) - 2, 0, 2, CGRectGetHeight(self.bounds))];
    [insetView setBackgroundColor:[UIColor colorWithRed:(1.0 - [self.comment.commentDepth intValue]*0.1) green:0.5f blue:0.5f alpha:1.0f]];
    [self addSubview:insetView];
    self.colorIndent = insetView;
}

-(void)resetProperties
{
	[self cleanStrongPointers];
	[self.parentHighlight removeFromSuperview];
	[self.headerLabel removeFromSuperview];
	[self.expandButton removeFromSuperview];
    [self.colorIndent removeFromSuperview];
	//clean any other properties
}

-(void)setupMainLabel
{
	if (!self.mainLabel) {
		float start = kStartingPositionX + kWidthOfBorder + 10;
		self.mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(start + [self insetWidth],
																   CGRectGetMaxY(self.headerLabel.frame),
																   self.frame.size.width - (2 * start) - [self insetWidth],
																   35)];
		[self.mainLabel setNumberOfLines:100];
		[self.mainLabel setFont:[PXCommentCell cellMainLabelFont]];
		[self.mainLabel setTextColor:[PXProbeHelpers colorWithRed:62 green:62 blue:62]];
		[self addSubview:self.mainLabel];
	}
}

-(int)insetWidth
{
	return [self.comment.commentDepth intValue] * 10;
}
		 
+(UIFont *)cellMainLabelFont
{
	return [UIFont fontWithName:kMainFont size:14];
}

-(void)cleanStrongPointers
{
	[super cleanStrongPointers];
	_comment = nil;
}

-(void)expandComments:(id)sender
{
	if ([self.delegate respondsToSelector:@selector(commentCellDidRequestExpand:)]) {
		[self.delegate commentCellDidRequestExpand:self.comment];
	}
}

@end
