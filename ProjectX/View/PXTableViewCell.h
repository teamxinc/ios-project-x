//
//  PXTableViewCell.h
//  ProjectX
//
//  Created by David Attaie on 13/02/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PXCardCell.h"
#import "ListingObject.h"

@interface PXTableViewCell : PXCardCell

@property (nonatomic, strong, readonly) ListingObject *listingObject;
@property (nonatomic, weak) UITableView *parent;

-(void)setupWithListingObject:(ListingObject *)listingObject;

@end
