//
//  PXLoginView.m
//  Probe
//
//  Created by David Attaie on 13/03/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "PXNetworkingManager.h"
#import "PXLoginView.h"

@interface PXLoginView()

@property (nonatomic, weak) UITextField *usernameTextField;
@property (nonatomic, weak) UITextField *passwordTextField;
@property (nonatomic, weak) UIButton *login;

@end

@implementation PXLoginView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupTextViews];
    }
    return self;
}

-(void)setupTextViews
{
	[self setBackgroundColor:[UIColor whiteColor]];
	UITextField *usernameTextField = [[UITextField alloc] initWithFrame:CGRectMake(35, 186, 250, 42)];
	[usernameTextField setBorderStyle:UITextBorderStyleNone];
	[usernameTextField setBackground:[UIImage imageNamed:@"textfield"]];
	[usernameTextField setPlaceholder:@"      Username"];
	[usernameTextField setFont:[UIFont fontWithName:kMainFont size:13]];
	[usernameTextField setAlpha:1.0f];
	[self addSubview:usernameTextField];
	self.usernameTextField = usernameTextField;
	
	UITextField *passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(35, 240, 250, 42)];
	[passwordTextField setBorderStyle:UITextBorderStyleNone];
	[passwordTextField setBackground:[UIImage imageNamed:@"textfield"]];
	[passwordTextField setPlaceholder:@"      Password"];
	[passwordTextField setFont:[UIFont fontWithName:kMainFont size:13]];
	[passwordTextField setAlpha:1.0f];
	[self addSubview:passwordTextField];
	self.passwordTextField = passwordTextField;
	
	UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[loginButton setBackgroundImage:[UIImage imageNamed:@"login_button"] forState:UIControlStateNormal];
	[loginButton addTarget:self action:@selector(loginSelected) forControlEvents:UIControlEventTouchUpInside];
	[loginButton setFrame:CGRectMake(35, 293, 250, 51)];
	[loginButton setAlpha:1.0f];
	[self addSubview:loginButton];
	self.login = loginButton;
}

-(void)loginSelected
{
	[MainNetworkingManager loginWithUsername:self.usernameTextField.text andPassword:self.passwordTextField.text];
}

@end
