//
//  PXCommentCell.h
//  Probe
//
//  Created by David Attaie on 05/05/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PXCardCell.h"
#import "Comment+Human.h"

@protocol PXCommentCellDelegate <NSObject>

@required
-(void)commentCellDidRequestExpand:(Comment *)comment;

@end

@interface PXCommentCell : PXCardCell

@property (nonatomic, weak) id<PXCommentCellDelegate> delegate;

-(void)setupWithComment:(Comment *)comment;
+(UIFont *)cellMainLabelFont;

@end
