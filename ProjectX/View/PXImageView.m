//
//  PXImageView.m
//  Probe
//
//  Created by David Attaie on 14/03/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "PXImageView.h"

@interface PXImageView()

@property (nonatomic, weak) UIImageView *imageView;

@end

@implementation PXImageView

-(id)init
{
	self = [super init];
	if (self) {
		[self setup];
	}
	return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self) {
		[self setup];
	}
	return self;
}

-(id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self setup];
	}
	return self;
}

-(void)setup
{
	[self setClipsToBounds:YES];
	UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.bounds];
	[imageView setTranslatesAutoresizingMaskIntoConstraints:NO];
	[imageView setContentMode:UIViewContentModeScaleAspectFill];
	[self addSubview:imageView];
	[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[image]|" options:0 metrics:Nil views:@{@"image": imageView}]];
	[self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[image]|" options:0 metrics:Nil views:@{@"image": imageView}]];
	self.imageView = imageView;
}

-(void)setImage:(UIImage *)image
{
	[self.imageView setImage:image];
}
@end
