//
//  PXTableViewCell.m
//  ProjectX
//
//  Created by David Attaie on 13/02/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "PXTableViewCell.h"
#import "ListingObject+Human.h"
#import "PXImageView.h"
#import "PXImageCache.h"

@interface PXTableViewCell()

@property (strong, nonatomic) UILabel *username;
@property (strong, nonatomic) UILabel *numberOfCommentsLabel;
@property (strong, nonatomic) UILabel *scoreLabel;
@property (strong, nonatomic) UILabel *subredditLabel;
@property (strong, nonatomic) PXImageView *cellImageView;
@property (nonatomic, strong, readwrite) ListingObject *listingObject;

@end

@implementation PXTableViewCell

-(void)setupWithListingObject:(ListingObject *)listingObject
{
	[self setSelectionStyle:UITableViewCellSelectionStyleNone];
	[self resetProperties];
	//setup view to be correct
	
	//initialize variables
	[self setupBackground];
	[self setupTopSection];
	[self setupUsernameLabel];
	[self setupMainLabel];
	[self setupCellImageView];
	
	switch ([[listingObject imageType] intValue]) {
		case ListingObjectTypeGif:
		case ListingObjectTypePhoto:
			[self setupPhotoFrame];
			[self.topView setBackgroundColor:[UIColor colorWithRed:16.0/255.0f green:131.0f/255.0f blue:152.0f/255.0f alpha:1.0f]];
			break;
		case ListingObjectTypeText:
		case ListingObjectTypeWebPage:
			[self.topView setBackgroundColor:[UIColor colorWithRed:98.0/255.0f green:195.0f/255.0f blue:180.0f/255.0f alpha:1.0f]];
			break;
		case ListingObjectTypeVideo:
			[self.topView setBackgroundColor:[UIColor colorWithRed:234.0/255.0f green:112.0f/255.0f blue:109.0f/255.0f alpha:1.0f]];
			break;
			
		default:
			break;
	}
	
	self.listingObject = listingObject;
	
	[self updateMainLabel:listingObject.title];
	[self.username setText:[NSString stringWithFormat:@"%@ • %@",listingObject.author, [self calculateLastTime]]];
	[self.scoreLabel setText:[NSString stringWithFormat:@"%@", listingObject.ups]];
	[self.numberOfCommentsLabel setText:[NSString stringWithFormat:@"%@", listingObject.downs]];
	[self.subredditLabel setText:listingObject.subreddit];
	
	UIImage *theImage = [[PXImageCache sharedPXImageCache] imageForKey:listingObject.url];
	if (theImage) {
		[self.cellImageView setImage:theImage];
	}else{
		[self.cellImageView setImage:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateImageNotification:) name:kImageDownloadedNotification object:nil];
	}
}

-(NSString *)calculateLastTime
{
	NSCalendar *sysCalendar = [NSCalendar currentCalendar];
	// Get conversion to months, days, hours, minutes
	unsigned int unitFlags = NSHourCalendarUnit | NSMinuteCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit;
	
	NSDateComponents *breakdownInfo = [sysCalendar components:unitFlags fromDate:self.listingObject.created_utc  toDate:[NSDate date]  options:0];
	
	if ([breakdownInfo day] > 0) {
		return [NSString stringWithFormat:@"%li days ago", (long)[breakdownInfo day]];
	}else if ([breakdownInfo hour] > 0) {
		return [NSString stringWithFormat:@"%li hours ago", (long)[breakdownInfo hour]];
	}else if ([breakdownInfo minute] > 0) {
		return [NSString stringWithFormat:@"%li minutes ago", (long)[breakdownInfo minute]];
	}
	return @"Just Now";
}

-(void)resetProperties
{
	[self.topView removeFromSuperview];
	self.topView = nil;
	[self.outlineLabel removeFromSuperview];
	self.outlineLabel = nil;
	[self.mainLabel removeFromSuperview];
	self.mainLabel = nil;
	[self.username removeFromSuperview];
	self.username = nil;
	[self.numberOfCommentsLabel removeFromSuperview];
	self.numberOfCommentsLabel = nil;
	[self.scoreLabel removeFromSuperview];
	self.scoreLabel = nil;
	[self.subredditLabel removeFromSuperview];
	self.subredditLabel = nil;
	[self.cellImageView removeFromSuperview];
	self.cellImageView = nil;
	[self.containerView removeFromSuperview];
	self.containerView = nil;
	self.listingObject = nil;
}

-(void)dealloc
{
	[_username removeFromSuperview];
	_username = nil;
	[_numberOfCommentsLabel removeFromSuperview];
	_numberOfCommentsLabel = nil;
	[_scoreLabel removeFromSuperview];
	_scoreLabel = nil;
	[_subredditLabel removeFromSuperview];
	_subredditLabel = nil;
	_listingObject = nil;
	[_cellImageView removeFromSuperview];
	_cellImageView = nil;
	[self cleanStrongPointers];
}

-(void)updateImageNotification:(NSNotification *)notification
{
	if ([notification.userInfo[@"imagekey"] isEqualToString: self.listingObject.url]) {
		dispatch_async(dispatch_get_main_queue(), ^{
			UIImage *theImage = [[PXImageCache sharedPXImageCache] imageForKey:self.listingObject.url];
			if (theImage){
				[self.cellImageView setImage:theImage];
				
					if ([self.parent indexPathForCell:self])
						[self.parent reloadRowsAtIndexPaths:@[[self.parent indexPathForCell:self]] withRowAnimation:UITableViewRowAnimationAutomatic];
			}
			if ([self.parent.visibleCells containsObject:self]) {
				[self.parent reloadRowsAtIndexPaths:@[[self.parent indexPathForCell:self]] withRowAnimation:UITableViewRowAnimationAutomatic];
			}
		});
	}
}

-(void)setupTopSection
{
	if (!self.topView) {
		[super setupTopSection];
		
		//setup arrows
		UIImageView *arrowsTop = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"upvotes"]];
		[self.topView addSubview:arrowsTop];
		UIImageView *arrowsDown = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"downvotes"]];
		[self.topView addSubview:arrowsDown];
		[arrowsTop setFrame:CGRectMake(2,
									   self.topView.frame.size.height/2 - self.topView.frame.size.height/4 - 1,
									   CGRectGetWidth(arrowsTop.frame)/2,
									   self.topView.frame.size.height/4)];
		[arrowsDown setFrame:CGRectMake(2,
										self.topView.frame.size.height/2 + 1,
										CGRectGetWidth(arrowsDown.frame)/2,
										self.topView.frame.size.height/4)];
		[arrowsTop setContentMode:UIViewContentModeScaleAspectFit];
		[arrowsDown setContentMode:UIViewContentModeScaleAspectFit];
		
		//setup score label
		UILabel *scoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(arrowsTop.frame),
																		self.topView.frame.size.height/4,
																		CGRectGetWidth(self.topView.bounds)/8,
																		self.topView.frame.size.height/2)];
		[scoreLabel setFont:[UIFont fontWithName:@"Bebas" size:17]];
		[scoreLabel setTextColor:[UIColor whiteColor]];
		[self.topView addSubview:scoreLabel];
		self.scoreLabel = scoreLabel;
		
		//setup comments icon
		UIImageView *commentsIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"comments"]];
		[self.topView addSubview:commentsIcon];
		[commentsIcon setFrame:CGRectMake(CGRectGetMaxX(self.scoreLabel.frame) + 20,
										  CGRectGetMidY(self.topView.bounds) - CGRectGetHeight(commentsIcon.frame)/4,
										  CGRectGetWidth(commentsIcon.frame)/2,
										  CGRectGetHeight(commentsIcon.frame)/2)];
		
		//setup comments label
		UILabel *commentsLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(commentsIcon.frame),
																		self.topView.frame.size.height/4,
																		CGRectGetWidth(self.topView.bounds)/10,
																		self.topView.frame.size.height/2)];
		[commentsLabel setFont:[UIFont fontWithName:@"Bebas" size:17]];
		[commentsLabel setTextColor:[UIColor whiteColor]];
		[self.topView addSubview:commentsLabel];
		self.numberOfCommentsLabel = commentsLabel;
		
		//setup subreddit label
		UILabel *subredditLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.topView.bounds) - CGRectGetWidth(self.topView.bounds)/2 - 10,
																		   self.topView.frame.size.height/4,
																		   CGRectGetWidth(self.topView.bounds)/2,
																		   self.topView.frame.size.height/2)];
		[subredditLabel setTextAlignment:NSTextAlignmentRight];
		[subredditLabel setFont:[UIFont fontWithName:@"Bebas" size:13]];
		[subredditLabel setTextColor:[UIColor whiteColor]];
		
		[self.topView addSubview:subredditLabel];
		self.subredditLabel = subredditLabel;
	}
}

-(void)setupPhotoFrame
{
	[self.cellImageView setFrame:CGRectMake(CGRectGetMinX(self.containerView.frame),
										   CGRectGetMaxY(self.mainLabel.frame),
										   CGRectGetWidth(self.containerView.frame),
										   CGRectGetMaxY(self.containerView.frame) - CGRectGetMaxY(self.mainLabel.frame))];
}

-(void)setupGifFrame
{
	[self setupPhotoFrame];
}

-(void)setupUsernameLabel
{
	if (!self.username) {
		float start = kStartingPositionX + kWidthOfBorder + 10;
		self.username = [[UILabel alloc] initWithFrame:CGRectMake(start,
																  CGRectGetHeight(self.topView.frame) + 15,
																  self.frame.size.width - (2 * start),
																  15)];
		[self.username setFont:[UIFont fontWithName:kMainFont size:13]];
		[self.username setTextColor:[PXProbeHelpers colorWithRed:162 green:162 blue:162]];
		[self addSubview:self.username];
	}
}

-(void)setupCellImageView
{
	if (!self.cellImageView) {
		self.cellImageView = [[PXImageView alloc] init];
		[self.cellImageView setContentMode:UIViewContentModeCenter];
		[self.cellImageView setClipsToBounds:YES];
		[self addSubview:self.cellImageView];
	}
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ([keyPath isEqualToString:@"image"]) {
		[self setNeedsUpdateConstraints];
		[self setNeedsDisplay];
		[[NSNotificationCenter defaultCenter] postNotificationName:kViewUpdateNotification object:nil];
	}
	[object removeObserver:self forKeyPath:keyPath];
}

-(void)updateMainLabel:(NSString *)withText
{
	[self.mainLabel setText:withText];
	[self.mainLabel sizeToFit];
}

@end
