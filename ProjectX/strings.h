//
//  strings.h
//  Probe
//
//  Created by David Attaie on 14/03/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#ifndef Probe_strings_h
#define Probe_strings_h

static NSString * kViewUpdateNotification = @"com.probe.viewupdateNotification";
static NSString * commentsCellReuse = @"com.probe.commentsCellReuse";

#endif
