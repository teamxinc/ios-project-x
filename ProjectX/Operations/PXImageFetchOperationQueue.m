//
//  PXImageFetchOperationQueue.m
//  Probe
//
//  Created by David Attaie on 10/06/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "PXImageFetchOperationQueue.h"
#import "PXImageFetchOperation.h"

@interface PXImageFetchOperationQueue()

@end

@implementation PXImageFetchOperationQueue

-(id)init{
	self = [super init];
	if (self) {
		self.maxConcurrentOperationCount = 3;
	}
	return self;
}

-(void)cancelOperationWithKey:(NSString *)key
{
	for (PXImageFetchOperation *operation in self.operations) {
		if ([operation.key isEqualToString:key]) {
			[operation cancel];
			NSLog(@"Cancelling Operation For Key %@", key);
			return;
		}
	}
}

@end
