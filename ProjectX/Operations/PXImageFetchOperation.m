//
//  PXImageFetchOperation.m
//  Probe
//
//  Created by David Attaie on 10/06/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "PXImageFetchOperation.h"
#import "PXImageCache.h"

@interface PXImageFetchOperation()

@property (nonatomic, strong, readwrite) NSString *key;

@end

@implementation PXImageFetchOperation

-(id)initWithKey:(NSString *)key
{
	self = [super init];
	if (self) {
		_key = key;
	}
	return self;
}

-(void)dealloc
{
	_key = nil;
}

-(void)main
{
	if (self.isCancelled)
		return;

	UIImage *imageInCache = [[PXImageCache sharedPXImageCache] objectForKey:self.key];
	
	if (imageInCache) {
		return;
	}
	
	if (self.isCancelled)
		return;
	
	NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.key]];
	
	if (imageData) {
		[[PXImageCache sharedPXImageCache] setObject:imageData forKey:self.key];
	}
}


@end
