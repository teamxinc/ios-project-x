//
//  PXImageFetchOperationQueue.h
//  Probe
//
//  Created by David Attaie on 10/06/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PXImageFetchOperationQueue : NSOperationQueue

-(void)cancelOperationWithKey:(NSString *)key;

@end
