//
//  NSString+Substrings.h
//  Probe
//
//  Created by David Attaie on 12/06/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Substrings)

-(BOOL)containsSubstring:(NSString *)substring;

@end
