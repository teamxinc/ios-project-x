//
//  NSString+Substrings.m
//  Probe
//
//  Created by David Attaie on 12/06/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "NSString+Substrings.h"

@implementation NSString (Substrings)

-(BOOL)containsSubstring:(NSString *)substring
{
	if ([self rangeOfString:substring].location == NSNotFound) {
		return NO;
	}
	return YES;
}

@end
