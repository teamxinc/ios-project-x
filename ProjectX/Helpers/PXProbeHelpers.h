//
//  PXProbeHelpers.h
//  Probe
//
//  Created by David Attaie on 03/04/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PXProbeHelpers : NSObject

+(id)validateDataInDictionary:(NSDictionary *)data forKey:(NSString *)key;
+(UIColor *)colorWithRed:(int)red green:(int)green blue:(int)blue;
+(NSString *)calculateLastTimeForDate:(NSDate *)date;

@end
