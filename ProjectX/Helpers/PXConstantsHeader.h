//
//  PXConstantsHeader.h
//  Probe
//
//  Created by David Attaie on 17/04/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#ifndef Probe_PXConstantsHeader_h
#define Probe_PXConstantsHeader_h

#define kNumberToFetch	8
static NSString * kImageDownloadedNotification = @"com.b4s.imageDownloaded";
static NSString * kCacheUpdatedNotification = @"com.b4s.CacheUpdated";
static NSString * kMainFont = @"HelveticaNeueLTPro-Cn";
static NSString * kMainFontMed = @"HelveticaNeueLTPro-MdCn";


#endif
