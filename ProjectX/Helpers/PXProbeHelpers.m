//
//  PXProbeHelpers.m
//  Probe
//
//  Created by David Attaie on 03/04/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "PXProbeHelpers.h"

@implementation PXProbeHelpers

+(id)validateDataInDictionary:(NSDictionary *)data forKey:(NSString *)key
{
	if (![data[key] isKindOfClass:[NSNull class]]) {
		return data[key];
	}
	return nil;
}

+(UIColor *)colorWithRed:(int)red green:(int)green blue:(int)blue
{
	return [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:1.0f];
}

+(NSString *)calculateLastTimeForDate:(NSDate *)date
{
	if (!date) {
		return @"Recently";
	}
	NSCalendar *sysCalendar = [NSCalendar currentCalendar];
	// Get conversion to months, days, hours, minutes
	unsigned int unitFlags = NSHourCalendarUnit | NSMinuteCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit;
	
	NSDateComponents *breakdownInfo = [sysCalendar components:unitFlags fromDate:date  toDate:[NSDate date]  options:0];
	
	if ([breakdownInfo day] > 0) {
		return [NSString stringWithFormat:@"%li days ago", (long)[breakdownInfo day]];
	}else if ([breakdownInfo hour] > 0) {
		return [NSString stringWithFormat:@"%li hours ago", (long)[breakdownInfo hour]];
	}else if ([breakdownInfo minute] > 0) {
		return [NSString stringWithFormat:@"%li minutes ago", (long)[breakdownInfo minute]];
	}
	return @"Just Now";
}

@end
