//
//  PXStaticHeader.h
//  ProjectX
//
//  Created by David Attaie on 13/02/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#ifndef ProjectX_PXStaticHeader_h
#define ProjectX_PXStaticHeader_h

enum {
	ListingObjectTypePhoto = 1,
	ListingObjectTypeText = 2,
	ListingObjectTypeVideo = 3,
	ListingObjectTypeGif = 4,
	ListingObjectTypeWebPage = 5
}typedef ListingObjectType;

#endif
