//
//  PXDetailDisplayController.h
//  Probe
//
//  Created by David Attaie on 05/05/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ListingObject+Human.h"

@interface PXDetailDisplayController : UIViewController

-(id)initWithListingObject:(ListingObject *)listingObject;

@end
