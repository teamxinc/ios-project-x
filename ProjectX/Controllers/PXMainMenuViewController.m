//
//  PXMainMenuViewController.m
//  Probe
//
//  Created by David Attaie on 18/06/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "PXSubredditsMenuTableViewCell.h"
#import "PXMainMenuViewController.h"
#import "PXNetworkingManager.h"
#import "PXProbeHelpers.h"

#define kNumberOfPages	1

@interface PXMainMenuViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) int currentPage;
@property (nonatomic, weak) UIPageControl *pageIndicator;
@property (nonatomic, weak) UITableView *subredditsTableView;
@property (nonatomic, strong) NSMutableArray *arrayOfSubreddits;

@end

@implementation PXMainMenuViewController


static NSString *subredditsMenuTVCReusableIdentifier = @"com.probe.reusableSubredditsTVC";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self setupArrayOfSubreddits];
	[self setupTopBar];
    [self setupCurrentPageIndicator];
	[self setupSubredditsTableView];
}

-(void)dealloc
{
	_arrayOfSubreddits = nil;
}

-(void)setupArrayOfSubreddits
{
	NSMutableArray *arrayOfSubreddits = [[NSUserDefaults standardUserDefaults] objectForKey:@"subreddits"];
	if (!arrayOfSubreddits) {
		arrayOfSubreddits = [NSMutableArray new];
		[arrayOfSubreddits addObjectsFromArray:@[@"pics", @"gifs", @"funny", @"front", @"leagueoflegends", @"earthporn"]];
	}
	self.arrayOfSubreddits = arrayOfSubreddits;
}

-(void)setupTopBar
{
	UIView *topBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 44)];
	[topBarView setBackgroundColor:[PXProbeHelpers colorWithRed:77 green:116 blue:133]];
	[self.view addSubview:topBarView];
	
	//setup closeButton
	UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[closeButton setFrame:CGRectMake(0,
									 0,
									 CGRectGetWidth(topBarView.bounds)/2,
									 CGRectGetHeight(topBarView.bounds))];
	[closeButton setImage:[UIImage imageNamed:@"closeIcon"] forState:UIControlStateNormal];
	[closeButton addTarget:self action:@selector(closeMenu:) forControlEvents:UIControlEventTouchUpInside];
	[closeButton setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.2f]];
//	[closeButton setContentEdgeInsets:UIEdgeInsetsMake(20, 0, 0, 0)];
	[topBarView addSubview:closeButton];
	
	//setup searchButton
	UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[searchButton setFrame:CGRectMake(CGRectGetWidth(topBarView.bounds)/2,
									  0,
									  CGRectGetWidth(topBarView.bounds)/2,
									  CGRectGetHeight(topBarView.bounds))];
	[searchButton setImage:[UIImage imageNamed:@"search_icon"] forState:UIControlStateNormal];
//	[searchButton setContentEdgeInsets:UIEdgeInsetsMake(20, 0, 0, 0)];
//	[searchButton addTarget:self action:@selector(closeMenu:) forControlEvents:UIControlEventTouchUpInside];
	[topBarView addSubview:searchButton];
}

-(void)closeMenu:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)setupCurrentPageIndicator
{
	UIPageControl *pageControl = [UIPageControl new];
	[pageControl setBackgroundColor:[UIColor darkGrayColor]];
	[pageControl setFrame:CGRectMake(0, 44, CGRectGetWidth(self.view.bounds), 20)];
	[pageControl setNumberOfPages:kNumberOfPages];
	[pageControl setCurrentPage:1];
	[self.view addSubview:pageControl];
	self.pageIndicator = pageControl;
}

-(void)setupSubredditsTableView
{
	UITableView *subredditsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,
																					 CGRectGetMaxY(self.pageIndicator.frame),
																					 CGRectGetWidth(self.view.bounds),
																					 CGRectGetHeight(self.view.bounds) - CGRectGetMaxY(self.pageIndicator.frame))];
	[subredditsTableView registerClass:[PXSubredditsMenuTableViewCell class] forCellReuseIdentifier:subredditsMenuTVCReusableIdentifier];
	[subredditsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	[subredditsTableView setDataSource:self];
	[subredditsTableView setDelegate:self];
	[self.view addSubview:subredditsTableView];
	self.subredditsTableView = subredditsTableView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	PXSubredditsMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:subredditsMenuTVCReusableIdentifier];
	[cell setupCellWithName:self.arrayOfSubreddits[indexPath.row]];
	return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 48.0f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.arrayOfSubreddits.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
	NSString *selectedSubreddit = self.arrayOfSubreddits[indexPath.row];
	if ([selectedSubreddit isEqualToString:@"front"]) {
		selectedSubreddit = @"";
	}
	[MainNetworkingManager setupCurrentSubreddit:selectedSubreddit];
	[self closeMenu:nil];
}

@end
