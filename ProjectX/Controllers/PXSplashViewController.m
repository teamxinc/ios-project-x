//
//  PXSplashViewController.m
//  Probe
//
//  Created by David Attaie on 25/02/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "PXSplashViewController.h"
#import	<QuartzCore/QuartzCore.h>
#import "PXFrontScreenViewController.h"

@interface PXSplashViewController ()

@property (nonatomic, weak) UIImageView *backgroundImageView;
@property (nonatomic, weak) UIButton *login;

@end

@implementation PXSplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self setupFirstLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupFirstLoad
{
	UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LaunchImage-568h@2x"]];
	[backgroundImageView setFrame:self.view.bounds];
	[self.view addSubview:backgroundImageView];
	self.backgroundImageView = backgroundImageView;
	[self animate];
}

-(void)animate
{
//	[UIView animateWithDuration:0.2f animations:^{
//		[self.backgroundImageView setAlpha:1.0f];
//	} completion:^(BOOL finished) {
//		[UIView animateWithDuration:1.0f animations:^{
//			[self.logoImageView setAlpha:1.0f];
//		} completion:^(BOOL finished) {
//			[self animatePush];
//		}];
//	}];
	[self performSelector:@selector(animatePush) withObject:nil afterDelay:1.0f];
}

-(void)animatePush
{
	[self.navigationController pushViewController:[PXFrontScreenViewController new] animated:NO];
}

@end
