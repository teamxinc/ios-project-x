//
//  PXDetailDisplayController.m
//  Probe
//
//  Created by David Attaie on 05/05/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "PXDetailDisplayController.h"
#import "PXNetworkingManager.h"
#import "PXCoreDataManager.h"
#import "PXProbeHelpers.h"
#import "PXCommentCell.h"
#import "PXImageCache.h"

@interface PXDetailDisplayController() <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UIWebViewDelegate, UIGestureRecognizerDelegate, PXCommentCellDelegate>

@property (nonatomic, strong) NSMutableArray *arrayOfComments;
@property (nonatomic, strong) ListingObject *listingObject;
@property (nonatomic, weak) UITableView *commentsTableView;
@property (nonatomic, weak) UIView *commentsView;
@property (nonatomic, weak) UIView *contentView;

@end

@implementation PXDetailDisplayController

-(id)initWithListingObject:(ListingObject *)listingObject
{
	self = [super init];
	if (self) {
		_listingObject = listingObject;
		_arrayOfComments = [NSMutableArray new];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(populateComments) name:NSManagedObjectContextDidSaveNotification object:nil];
		[self populateComments];
		[self setupMainView];
		[self setupTableView];
	}
	return self;
}

-(void)dealloc
{	_listingObject = nil;
	_arrayOfComments = nil;
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)setupMainView
{
	[self.view setBackgroundColor:[UIColor colorWithWhite:0.1f alpha:1.0f]];
	if ([self.listingObject.imageType intValue] == ListingObjectTypePhoto) {
		UIImage *mainImage = [[PXImageCache sharedPXImageCache] imageForKey:self.listingObject.url];
		
		CGFloat width = MIN(mainImage.size.width/2, CGRectGetWidth(self.view.bounds));
		CGFloat height = MIN(mainImage.size.height/2, CGRectGetHeight(self.view.bounds));
		
		UIImageView *mainImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.view.bounds)/2 - width/2,
																				   CGRectGetHeight(self.view.bounds)/2 - height/2,
																				   width,
																				   height)];
		[mainImageView setContentMode:UIViewContentModeScaleAspectFit];
		[mainImageView setImage:mainImage];
		[self.view addSubview:mainImageView];
	}
	
	if ([self.listingObject.imageType intValue] == ListingObjectTypeText) {
//		if (self.listingObject.text_html) {
//			UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0,
//																			 0,
//																			 CGRectGetWidth(self.view.bounds),
//																			 CGRectGetHeight(self.view.bounds) - 44)];
//			
//			[webView setBackgroundColor:[UIColor clearColor]];
//			NSData *stringData = [self.listingObject.text_html dataUsingEncoding:NSUTF8StringEncoding];
//			[webView loadData:stringData MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:nil];
//			[webView setDelegate:self];
//			[self.view addSubview:webView];
//		}else{
			UITextView *textField = [[UITextView alloc] initWithFrame:CGRectMake(0,
																				  0,
																				  CGRectGetWidth(self.view.bounds),
																				  CGRectGetHeight(self.view.bounds) - 44)];
			[textField setTextColor:[UIColor whiteColor]];
			[textField setFont:[UIFont fontWithName:kMainFont size:14]];
			[textField setText:self.listingObject.text];
			[textField setBackgroundColor:[UIColor clearColor]];
			[self.view addSubview:textField];
//		}
	}
	
	if ([self.listingObject.imageType intValue] == ListingObjectTypeWebPage || [self.listingObject.imageType intValue] == ListingObjectTypeGif) {
		UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0,
																		 0,
																		 CGRectGetWidth(self.view.bounds),
																		 CGRectGetHeight(self.view.bounds) - 44)];

		[webView setBackgroundColor:[UIColor clearColor]];
		NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:self.listingObject.url]];
		[webView loadRequest:urlRequest];
		[webView setDelegate:self];
		[self.view addSubview:webView];
	}

	
	UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panMainView:)];
	[self.view addGestureRecognizer:panGesture];
	[panGesture setDelegate:self];
}

-(void)setupHeader
{
	UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[backButton setImage:[UIImage imageNamed:@"downArrowComments"] forState:UIControlStateNormal];
	[backButton addTarget:self action:@selector(resetCommentsView:) forControlEvents:UIControlEventTouchUpInside];
	[backButton setFrame:CGRectMake(CGRectGetWidth(self.commentsView.frame) - 30, 20, 23, 12)];
	[self.commentsView addSubview:backButton];
	
	UIImageView *commentsImage = [[UIImageView alloc] initWithFrame:CGRectMake(27, 25 - 8, 17, 15)];
	[commentsImage setImage:[UIImage imageNamed:@"messaging_icon"]];
	[self.commentsView addSubview:commentsImage];
	
	UILabel *numberOfCommentsLabel = [[UILabel alloc] initWithFrame:CGRectMake(55, CGRectGetMinY(commentsImage.frame), 150, CGRectGetHeight(commentsImage.frame))];
	[numberOfCommentsLabel setFont:[UIFont fontWithName:kMainFont size:14]];
	[numberOfCommentsLabel setText:[NSString stringWithFormat:@"%li comments", [self.listingObject.numComments longValue]]];
	[numberOfCommentsLabel setTextColor:[UIColor whiteColor]];
	[self.commentsView addSubview:numberOfCommentsLabel];
}

-(void)resetCommentsView:(id)sender
{
	if (CGRectGetMinY(self.commentsView.frame) < CGRectGetMidY(self.view.bounds)) {
		[UIView animateWithDuration:0.2f animations:^{
			[self.commentsView setFrame:CGRectMake(0,
												   CGRectGetHeight(self.view.bounds) - 50,
												   CGRectGetWidth(self.commentsView.frame),
												   CGRectGetHeight(self.commentsView.frame))];
		} completion:^(BOOL finished) {
			
		}];
	}else{
		[UIView animateWithDuration:0.2f animations:^{
			[self.commentsView setFrame:CGRectMake(0,
												   50,
												   CGRectGetWidth(self.commentsView.frame),
												   CGRectGetHeight(self.commentsView.frame))];
		} completion:^(BOOL finished) {
			
		}];
	}
}

-(void)setupTableView
{
	UIView *commentsView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.bounds) - 50, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - 50)];
	[commentsView setBackgroundColor:[PXProbeHelpers colorWithRed:111 green:196 blue:179]];
	UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(commentsViewDidPan:)];
	[self.view addSubview:commentsView];
	[commentsView addGestureRecognizer:pan];
	self.commentsView = commentsView;
	[self setupHeader];
	
	UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 50, CGRectGetWidth(commentsView.bounds), CGRectGetHeight(commentsView.bounds) - 20)];
	tableView.contentInset = UIEdgeInsetsMake(0, 0, -20, 0);
	[tableView registerClass:[PXCommentCell class] forCellReuseIdentifier:commentsCellReuse];
	[commentsView addSubview:tableView];
	self.commentsTableView = tableView;
	[self.commentsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	[self.commentsTableView setDataSource:self];
	[self.commentsTableView setDelegate:self];
	
	[self setupContentView];
}

-(void)setupContentView
{
	UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMinY(self.commentsView.frame) - 144, 320, 144)];
	[contentView setBackgroundColor:[UIColor clearColor]];
	[self.view addSubview:contentView];
	
	UIImageView *gradient = [[UIImageView alloc] initWithFrame:contentView.bounds];
	[gradient setImage:[UIImage imageNamed:@"gradient_overlay"]];
	[contentView addSubview:gradient];
	
	UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 20, CGRectGetWidth(contentView.bounds) - 100, CGRectGetHeight(contentView.bounds) - 40)];
	[titleLabel setFont:[UIFont fontWithName:kMainFont size:17]];
	[titleLabel setTextColor:[UIColor whiteColor]];
	[titleLabel setText:self.listingObject.title];
	[titleLabel setNumberOfLines:5];
	[contentView addSubview:titleLabel];
	[titleLabel sizeToFit];
	[titleLabel setFrame:CGRectMake(CGRectGetMinX(titleLabel.frame),
									CGRectGetMidY(contentView.bounds) - (CGRectGetHeight(titleLabel.frame)/2),
									CGRectGetWidth(titleLabel.frame),
									CGRectGetHeight(titleLabel.frame))];
	
	
	UILabel *upvotesLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMidY(titleLabel.frame) - 10, CGRectGetMinX(titleLabel.frame) - 10, 25)];
	[upvotesLabel setFont:[UIFont fontWithName:kMainFontMed size:22]];
	[upvotesLabel setTextColor:[UIColor whiteColor]];
	[upvotesLabel setText:[NSString stringWithFormat:@"%@", self.listingObject.ups]];
	[upvotesLabel setNumberOfLines:5];
	[upvotesLabel setTextAlignment:NSTextAlignmentCenter];
	[contentView addSubview:upvotesLabel];
	
	UILabel *upvotesLabelDetail = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(upvotesLabel.frame), CGRectGetMinX(titleLabel.frame) - 10, 15)];
	[upvotesLabelDetail setFont:[UIFont fontWithName:kMainFontMed size:9]];
	[upvotesLabelDetail setTextColor:[UIColor whiteColor]];
	[upvotesLabelDetail setText:[NSString stringWithFormat:@"UPVOTES"]];
	[upvotesLabelDetail setNumberOfLines:5];
	[upvotesLabelDetail setTextAlignment:NSTextAlignmentCenter];
	[contentView addSubview:upvotesLabelDetail];
	
	UILabel *userLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(titleLabel.frame), CGRectGetMaxY(titleLabel.frame), CGRectGetWidth(contentView.bounds) - CGRectGetMinX(titleLabel.frame) - 10, 20)];
	[userLabel setFont:[UIFont fontWithName:kMainFont size:10]];
	[userLabel setTextColor:[UIColor grayColor]];
	
	NSString *usernameAndDate = [NSString stringWithFormat:@"%@ • %@", self.listingObject.author, [PXProbeHelpers calculateLastTimeForDate:self.listingObject.date_added]];
	[userLabel setText:usernameAndDate];
	[contentView addSubview:userLabel];
	
	UIView *barView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(upvotesLabel.frame),
															   CGRectGetMinY(titleLabel.frame) - 3,
															   1,
															   MAX(CGRectGetMaxY(upvotesLabelDetail.frame), CGRectGetMaxY(userLabel.frame)) - CGRectGetMinY(titleLabel.frame) + 6)];
	[barView setBackgroundColor:[UIColor whiteColor]];
	[contentView addSubview:barView];

	self.contentView = contentView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	PXCommentCell *tableViewCell = [tableView dequeueReusableCellWithIdentifier:commentsCellReuse];
	[self configureCell:tableViewCell atIndexPath:indexPath];
	
	return tableViewCell;
}

-(void)configureCell:(PXCommentCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	[[NSOperationQueue mainQueue] addOperationWithBlock:^{
		Comment *comment = self.arrayOfComments[indexPath.row];
		[cell setupWithComment:comment];
		[cell setDelegate:self];
	}];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.arrayOfComments.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	Comment *comment = self.arrayOfComments[indexPath.row];
	return [self sizeOfTextLabelForText:comment.text withComment:comment] + 50;
}

-(CGFloat)sizeOfTextLabelForText:(NSString *)text withComment:(Comment *)comment
{
	UILabel *sizer = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 284 - ([comment.commentDepth intValue] * 10), self.view.frame.size.height)];
	[sizer setNumberOfLines:100];
	[sizer setFont:[PXCommentCell cellMainLabelFont]];
	[sizer setText:text];
	[sizer sizeToFit];
	return CGRectGetHeight(sizer.frame);
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	CGFloat sectionHeaderHeight = 45;
	
	if (scrollView.contentOffset.y <= sectionHeaderHeight && scrollView.contentOffset.y >= 0) {
		scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
	}
	if (scrollView.contentOffset.y > sectionHeaderHeight) {
		scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
	}
	
	CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
	
    float reload_distance = 500;
    if(y >= h - reload_distance) {
        [self loadMoreRows];
    }
}

-(void)loadMoreRows
{
	[MainNetworkingManager fetchMoreComments];
}


-(void)populateComments
{
	[[NSOperationQueue mainQueue] addOperationWithBlock:^{
		NSManagedObjectContext *updateContext = [MainCoreDataManager mainContext];
		NSFetchRequest *request = [[NSFetchRequest alloc] init];
		NSEntityDescription *description = [NSEntityDescription entityForName:@"Comment" inManagedObjectContext: updateContext];
		
		NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"date_added" ascending:YES];
		[request setFetchBatchSize:kNumberToFetch];
		[request setSortDescriptors:@[sortDescriptor]];
		
		NSPredicate *predicate = [NSPredicate predicateWithFormat:@"parentListingObject == %@ && date_added != nil", self.listingObject];
		[request setPredicate:predicate];
		
		[request setEntity:description];
		[request setShouldRefreshRefetchedObjects:YES];
		
		NSError *error = nil;
		NSArray *results = [updateContext executeFetchRequest:request error:&error];
		if (!error) {
			[self.arrayOfComments removeAllObjects];
			for (int i = 0; i < results.count; i++) {
				Comment *comment = results[i];
				[self.arrayOfComments addObject:comment];
				if ([comment.expanded boolValue]) {
					[self addChildCommentsForComment:comment toArray:self.arrayOfComments];
				}
			}
			[self.commentsTableView reloadData];
		}
	}];
}

-(void)addChildCommentsForComment:(Comment *)comment toArray:(NSMutableArray *)array
{
	for (Comment *child in comment.children) {
		[array addObject:child];
		if (child.children.count > 0 && [child.expanded boolValue]) {
			[self addChildCommentsForComment:child toArray:array];
		}
	}
}

-(void)commentsViewDidPan:(id)sender
{
	UIPanGestureRecognizer *pan = sender;
	if (pan.state == UIGestureRecognizerStateEnded) {
		if (CGRectGetMinY(self.commentsView.frame) > CGRectGetMidY(self.view.bounds)) {
			[UIView animateWithDuration:0.2f animations:^{
				[self.commentsView setFrame:CGRectMake(0,
													   CGRectGetHeight(self.view.bounds) - 50,
													   CGRectGetWidth(self.commentsView.frame),
													   CGRectGetHeight(self.commentsView.frame))];
				[self.contentView setFrame:CGRectMake(0,
													  CGRectGetMinY(self.commentsView.frame) - CGRectGetHeight(self.contentView.frame),
													  CGRectGetWidth(self.contentView.frame),
													  CGRectGetHeight(self.contentView.frame))];
			} completion:^(BOOL finished) {
				
			}];
		}else{
			[UIView animateWithDuration:0.2f animations:^{
				[self.commentsView setFrame:CGRectMake(0,
													   50,
													   CGRectGetWidth(self.commentsView.frame),
													   CGRectGetHeight(self.commentsView.frame))];
				[self.contentView setFrame:CGRectMake(0,
													  CGRectGetMinY(self.commentsView.frame) - CGRectGetHeight(self.contentView.frame),
													  CGRectGetWidth(self.contentView.frame),
													  CGRectGetHeight(self.contentView.frame))];
			} completion:^(BOOL finished) {
				
			}];
		}
	}else{
		if (CGRectGetMinY(self.commentsView.frame) < 50) {
			[self.commentsView setFrame:CGRectMake(0,
												   50,
												   CGRectGetWidth(self.commentsView.bounds),
												   CGRectGetHeight(self.commentsView.bounds))];
		}else if(CGRectGetMinY(self.commentsView.frame) > (CGRectGetHeight(self.view.bounds) - 50)){
			[self.commentsView setFrame:CGRectMake(0,
												   (CGRectGetHeight(self.view.bounds) - 50),
												   CGRectGetWidth(self.commentsView.bounds),
												   CGRectGetHeight(self.commentsView.bounds))];
		}else{
			UIPanGestureRecognizer *pan = sender;
			[self.commentsView setFrame:CGRectMake(0,
												   [pan locationInView:self.view].y,
												   CGRectGetWidth(self.commentsView.bounds),
												   CGRectGetHeight(self.commentsView.bounds))];
		}
		[self.contentView setFrame:CGRectMake(0,
											  CGRectGetMinY(self.commentsView.frame) - CGRectGetHeight(self.contentView.frame),
											  CGRectGetWidth(self.contentView.frame),
											  CGRectGetHeight(self.contentView.frame))];
	}
}

-(void)panMainView:(id)sender
{
	UIPanGestureRecognizer *pan = sender;
	if (pan.state != UIGestureRecognizerStateEnded) {
		CGPoint velocity = [pan velocityInView:pan.view];
		
		if(velocity.x > 0)
		{
			if (CGRectGetMinX(self.view.frame) > CGRectGetWidth(self.view.frame) - 50) {
				[self.view setFrame:CGRectMake(CGRectGetWidth(self.view.frame), CGRectGetMinY(self.view.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
			}else if (CGRectGetMinX(self.view.frame) < 0) {
				[self.view setFrame:CGRectMake(0, CGRectGetMinY(self.view.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
			}else{
				[self.view setFrame:CGRectMake([pan locationInView:self.view.superview].x, CGRectGetMinY(self.view.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
			}
		}

	}else{
		if (pan.state == UIGestureRecognizerStateEnded) {
			if (CGRectGetMinX(self.view.frame) > 0 && CGRectGetMinX(self.view.frame) < CGRectGetWidth(self.view.frame) - 50) {
				[UIView animateWithDuration:0.2f animations:^{
					[self.view setFrame:CGRectMake(0, CGRectGetMinY(self.view.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
				} completion:^(BOOL finished) {
					
				}];
			}else {
				[UIView animateWithDuration:0.2f animations:^{
					[self.view setFrame:CGRectMake(CGRectGetWidth(self.view.frame), CGRectGetMinY(self.view.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
				} completion:^(BOOL finished) {
					[self.view removeFromSuperview];
				}];
			}
		}
	}
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
	return YES;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
	CGPoint positionInViewCurrently = [touch locationInView:self.view];
	if (positionInViewCurrently.x < 10) {
		return YES;
	}

	return NO;
}

#pragma mark - PXCommentCell Delegate

-(void)commentCellDidRequestExpand:(Comment *)comment
{
	if ([comment.expanded boolValue]) {
		comment.expanded = [NSNumber numberWithBool:NO];
	}else{
		comment.expanded = [NSNumber numberWithBool:YES];
	}
	[self populateComments];
}

@end
