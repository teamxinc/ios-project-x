//
//  PXFrontScreenViewController.m
//  ProjectX
//
//  Created by David Attaie on 06/02/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "PXFrontScreenViewController.h"
#import "PXDetailDisplayController.h"
#import "PXCoreDataManager.h"
#import "ListingObject+Human.h"
//#import "PXCardView.h"
#import "PXImageCache.h"
#import "PXNetworkingManager.h"
#import "PXTableViewCell.h"
#import "PXLoginView.h"
#import "PXMainMenuViewController.h"

@interface PXFrontScreenViewController () <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property (nonatomic, weak) UIScrollView *ourScrollView;
@property (nonatomic, weak) UITableView *ourTableView;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSMutableArray *arrayOfLoadingCells;
@property (nonatomic, weak) UIView *changeSubredditView;
@property (nonatomic, weak) UITextField *changeSubredditTextField;
@property (nonatomic, strong) PXDetailDisplayController *displayController;

@end

@implementation PXFrontScreenViewController

static NSString *photoCellReuseIdentifer = @"com.probe.photocellreuse";
static NSString *textCellReuseIdentifier = @"com.probe.textcellreuse";
static NSString *gifCellReuseIdentifier = @"com.probe.gifcellreuse";
static NSString *videoCellReuseIdentifier = @"com.probe.videocellreuse";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTableView) name:kViewUpdateNotification object:nil];
		_arrayOfLoadingCells = [NSMutableArray new];
		[self populateListingObjects];
    }
    return self;
}

-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kViewUpdateNotification object:nil];
	_arrayOfLoadingCells = nil;
}

-(void)setupOurScrollView
{
	UIScrollView *ourScrollView = [UIScrollView new];
	[ourScrollView setBackgroundColor:[UIColor lightGrayColor]];
	[ourScrollView setFrame:self.view.frame];
	[ourScrollView setContentSize:CGSizeMake(310, 0)];
	[self.view addSubview:ourScrollView];
	self.ourScrollView = ourScrollView;
}

-(void)updateTableView
{
	[self.ourTableView reloadData];
}

-(void)setupTableView
{
	UITableView *tableView = [UITableView new];
	[tableView setBackgroundColor:[UIColor colorWithWhite:0.953f alpha:1.0f]];
	[tableView setFrame:CGRectMake(0, 44, self.view.frame.size.width, self.view.frame.size.height - 44)];
	[self.view addSubview:tableView];
	[tableView setDelegate:self];
	[tableView setDataSource:self];
	[tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	[tableView setBounces:YES];
	[tableView setShowsVerticalScrollIndicator:NO];
	tableView.contentInset = UIEdgeInsetsMake(0, 0, -20, 0);
	
	[tableView registerClass:[PXTableViewCell class] forCellReuseIdentifier:photoCellReuseIdentifer];
	[tableView registerClass:[PXTableViewCell class] forCellReuseIdentifier:gifCellReuseIdentifier];
	[tableView registerClass:[PXTableViewCell class] forCellReuseIdentifier:videoCellReuseIdentifier];
	[tableView registerClass:[PXTableViewCell class] forCellReuseIdentifier:textCellReuseIdentifier];
	
	UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
	[refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
	[tableView addSubview:refreshControl];
	
	self.ourTableView = tableView;
}

-(void)setupTopView
{
	UIView *topBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 44)];
	[topBarView setBackgroundColor:[PXProbeHelpers colorWithRed:77 green:116 blue:133]];
	[self.view addSubview:topBarView];
	
	//setup closeButton
	UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[menuButton setFrame:CGRectMake(0,
									 0,
									 CGRectGetWidth(topBarView.bounds)/2,
									 CGRectGetHeight(topBarView.bounds))];
	[menuButton setImage:[UIImage imageNamed:@"menu_icon"] forState:UIControlStateNormal];
	[menuButton addTarget:self action:@selector(showMenu:) forControlEvents:UIControlEventTouchUpInside];
	[menuButton setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.2f]];
	[topBarView addSubview:menuButton];
	
	//setup searchButton
	UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[searchButton setFrame:CGRectMake(CGRectGetWidth(topBarView.bounds)/2,
									  0,
									  CGRectGetWidth(topBarView.bounds)/2,
									  CGRectGetHeight(topBarView.bounds))];
	[searchButton setImage:[UIImage imageNamed:@"search_icon"] forState:UIControlStateNormal];
	[topBarView addSubview:searchButton];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self setupTableView];
	[self setupTopView];
	[[NSNotificationCenter defaultCenter] addObserverForName:kCacheUpdatedNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
		[self.ourTableView reloadData];
	}];
}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[self refresh:nil];
//	if (![MainNetworkingManager currentSubreddit] && ![MainNetworkingManager.currentSubreddit isEqualToString:@""]) {
//		[self.currentStatusLabel setText:@"Front Page"];
//	}else{
//		[self.currentStatusLabel setText:[MainNetworkingManager currentSubreddit]];
//	}
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	PXTableViewCell *tableViewCell;
	ListingObject *listingObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
	
	switch ([listingObject.imageType intValue]) {
		case ListingObjectTypeGif:
		case ListingObjectTypePhoto:
		{
			tableViewCell = (PXTableViewCell *)[self.ourTableView dequeueReusableCellWithIdentifier:photoCellReuseIdentifer];
			[tableViewCell setBackgroundColor:[UIColor clearColor]];
		}
			break;
		case ListingObjectTypeText:
		case ListingObjectTypeWebPage:
		{
			tableViewCell = (PXTableViewCell *)[self.ourTableView dequeueReusableCellWithIdentifier:textCellReuseIdentifier];
			[tableViewCell setBackgroundColor:[UIColor clearColor]];
		}
			break;
		case ListingObjectTypeVideo:
		{
			tableViewCell = (PXTableViewCell *)[self.ourTableView dequeueReusableCellWithIdentifier:videoCellReuseIdentifier];
			[tableViewCell setBackgroundColor:[UIColor clearColor]];
		}
			break;
			
		default:
			break;
	}
	[self configureCell:tableViewCell atIndexPath:indexPath];

	return tableViewCell;
}

-(void)configureCell:(PXTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	[[NSOperationQueue mainQueue] addOperationWithBlock:^{
		ListingObject *listingObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
		[cell setParent:self.ourTableView];
		[cell setupWithListingObject:listingObject];
	}];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
	return 45.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 45.0f)];
	[headerView setBackgroundColor:[UIColor colorWithRed:111/255.0f green:196/255.0f blue:179/255.0f alpha:1.0f]];
	
	[self addButtonWithTitle:@"ALL" toView:headerView withLastPoint:0];
	[self addButtonWithTitle:@"IMAGES" toView:headerView withLastPoint:64];
	[self addButtonWithTitle:@"GIFS" toView:headerView withLastPoint:64*2];
	[self addButtonWithTitle:@"VIDEOS" toView:headerView withLastPoint:64*3];
	[self addButtonWithTitle:@"ARTICLES" toView:headerView withLastPoint:64*4];
	
	return headerView;
}

-(UIButton *)addButtonWithTitle:(NSString *)title toView:(UIView *)viewToAddTo withLastPoint:(float)lastXPoint
{
	UIButton *allButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[allButton setTitle:title forState:UIControlStateNormal];
	[allButton.titleLabel setFont:[UIFont fontWithName:@"Bebas" size:12]];
	[allButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[allButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateSelected];
	[allButton setFrame:CGRectMake(CGRectGetMinX(viewToAddTo.bounds) + lastXPoint, CGRectGetMinY(viewToAddTo.bounds), 64, CGRectGetHeight(viewToAddTo.bounds))];
	[viewToAddTo addSubview:allButton];
	return allButton;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	ListingObject *listingObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
	switch ([listingObject.imageType intValue]) {
		case ListingObjectTypeVideo:
			return 150.0f;
			break;
		case ListingObjectTypeText:
		case ListingObjectTypeWebPage:
			return [self sizeOfTextLabelForText:listingObject.title] + 80;
			break;
		case ListingObjectTypeGif:
		case ListingObjectTypePhoto:
		{
			if (listingObject.imageHeight) {
				return MIN([listingObject.imageHeight intValue] + 120.0f, CGRectGetHeight(tableView.bounds));
			}
			UIImage *imageFromCache = [[PXImageCache sharedPXImageCache] imageForKey:listingObject.url];
			if (imageFromCache) {
				return MIN(imageFromCache.size.height/2 + 120.0f, CGRectGetHeight(tableView.bounds));
			}
			return 150.0f;
		}
			break;
			
		default:
			break;
	}
	return 100.0f;
}

-(CGFloat)sizeOfTextLabelForText:(NSString *)text
{
	float start = kStartingPositionX + kWidthOfBorder + 10;
	UILabel *sizer = [[UILabel alloc] initWithFrame:CGRectMake(0,
															   0,
															   self.view.frame.size.width - (2 * start),
															   150)];
	[sizer setNumberOfLines:4];
	[sizer setFont:[UIFont fontWithName:kMainFont size:17]];
	[sizer setText:text];
	[sizer sizeToFit];
	return CGRectGetHeight(sizer.frame);
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if ([[self.fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    } else
        return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	ListingObject *listingObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
	[MainNetworkingManager fetchCommentsForListingObject:listingObject];
	PXDetailDisplayController *displayController = [[PXDetailDisplayController alloc] initWithListingObject:listingObject];
	self.displayController = displayController;
	
	[displayController.view setFrame:CGRectMake(CGRectGetWidth(self.view.bounds), 0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds))];
	[self.view addSubview:displayController.view];
	[UIView animateWithDuration:0.2f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
		[displayController.view setFrame:self.view.bounds];
	} completion:nil];
}

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
	[self loadCurrentImages];
}

-(void)loadCurrentImages
{
	NSMutableArray *objectsToRemove = [[NSMutableArray alloc] initWithArray:self.arrayOfLoadingCells];
	
	NSArray *cellsToLoad = [self.ourTableView visibleCells];
	for (PXTableViewCell *cell in cellsToLoad) {
		if ([objectsToRemove containsObject:cell]) {
			[objectsToRemove removeObject:cell];
			[self.arrayOfLoadingCells removeObject:cell];
		}else{
			[MainNetworkingManager fetchImageForKey:cell.listingObject.url];
			[self.arrayOfLoadingCells addObject:cell];
		}
	}
	
	for (PXTableViewCell *cell in objectsToRemove) {
		[MainNetworkingManager cancelImageForKey:cell.listingObject.url];
	}
}

-(void)userTappedCurrentStatus:(id)sender
{
	//show window
	[self setupAndShowRenameView];
}

-(void)setupAndShowRenameView
{
	UIView *renameView = [[UIView alloc] initWithFrame:self.view.bounds];
	[renameView setBackgroundColor:[UIColor colorWithWhite:0.0f alpha:0.5f]];
	
	UITapGestureRecognizer *dismissGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissChangeSubredditView:)];
	[renameView addGestureRecognizer:dismissGesture];
	
	//setup name textView
	UITextField *renameTextField = [UITextField new];
	[renameTextField setFrame:CGRectMake(CGRectGetMinX(self.view.bounds) + 20,
										CGRectGetMidY(self.view.bounds) + 20,
										CGRectGetWidth(self.view.bounds) - 40,
										 40)];
	[renameTextField setBackgroundColor:[UIColor whiteColor]];
	if ([MainNetworkingManager currentSubreddit]) {
		[renameTextField setText:[MainNetworkingManager currentSubreddit]];
	}else{
		[renameTextField setText:@"Front Page"];
	}
	
	[renameView addSubview:renameTextField];
	self.changeSubredditTextField = renameTextField;

	[self.view addSubview:renameView];
	self.changeSubredditView = renameView;
}

-(void)dismissChangeSubredditView:(id)sender
{
	[MainNetworkingManager setupCurrentSubreddit:self.changeSubredditTextField.text];
//	[self.currentStatusLabel setText:self.changeSubredditTextField.text];
	
	//cleanup
	[self.changeSubredditView removeFromSuperview];
	self.changeSubredditView = nil;
	[self.changeSubredditTextField removeFromSuperview];
	self.changeSubredditTextField = nil;
	
	//reload page
	[MainNetworkingManager fetchFrontScreenWithCompletion:nil];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	CGFloat sectionHeaderHeight = 45;

	if (scrollView.contentOffset.y <= sectionHeaderHeight && scrollView.contentOffset.y >= 0) {
		scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
	}
	if (scrollView.contentOffset.y > sectionHeaderHeight) {
		scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
	}
	
	CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
	
    float reload_distance = 500;
    if(y >= h - reload_distance) {
        [self loadMoreRows];
    }
}

-(void)loadMoreRows
{
	[MainNetworkingManager fetchMore];
}

-(void)refresh:(id)sender
{
	[MainNetworkingManager fetchFrontScreenWithCompletion:^{
		UIRefreshControl *refreshController = sender;
		[refreshController endRefreshing];
	}];
}

-(void)populateListingObjects
{
	NSManagedObjectContext *updateContext = [MainCoreDataManager mainContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *description = [NSEntityDescription entityForName:@"ListingObject" inManagedObjectContext: updateContext];

    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"date_added" ascending:YES];
	[request setPredicate:[NSPredicate predicateWithFormat:@"date_added != nil"]];
    [request setFetchBatchSize:kNumberToFetch];
    [request setSortDescriptors:@[sortDescriptor]];
    [request setEntity:description];
	[request setShouldRefreshRefetchedObjects:YES];

    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                                               managedObjectContext:updateContext
                                                                                                 sectionNameKeyPath:nil
                                                                                                          cacheName:nil];
    fetchedResultsController.delegate = self;
    NSError *error = nil;
    [fetchedResultsController performFetch:&error];
    if (error) {
        NSLog(@"Error %@", error.localizedDescription);
    }

    self.fetchedResultsController = fetchedResultsController;

}

-(void)showMenu:(id)sender
{
	PXMainMenuViewController *mainMenu = [[PXMainMenuViewController alloc] init];
	[self.navigationController pushViewController:mainMenu animated:YES];
}

#pragma mark - Fetched Results Delegate

-(void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
//	[[NSOperationQueue mainQueue] addOperationWithBlock:^{
		[self.ourTableView beginUpdates];
//	}];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
//    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
		[self loadCurrentImages];
		[self.ourTableView endUpdates];
//	}];
}

-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
//	[[NSOperationQueue mainQueue] addOperationWithBlock:^{
		switch (type) {
			case NSFetchedResultsChangeInsert:
					[self.ourTableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
				break;
			case NSFetchedResultsChangeDelete:
					[self.ourTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
				break;
			case NSFetchedResultsChangeUpdate:
					[self configureCell:(PXTableViewCell *)[self.ourTableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
				break;
			case NSFetchedResultsChangeMove:
					[self.ourTableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
					[self.ourTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
				break;
			default:
				break;
		}
//	}];
}

@end
