//
//  PXNetworkingManager.m
//  ProjectX
//
//  Created by David Attaie on 26/01/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "PXNetworkingManager.h"
#import "PXModelCache.h"
#import "PXImageCache.h"
#import "ListingObject+Human.h"
#import "Comment+Human.h"
#import "PXCoreDataManager.h"
#import "PXImageFetchOperation.h"
#import <ImageIO/ImageIO.h>

#define kUserAgent		@"ProjectX PreAlpha 1.0.0"
#define kDevelopmentKey @""
#define kBaseURL		@"http://api.reddit.com"
#define kDefaultTimeout 30

@interface PXNetworkingManager()

@property (nonatomic, strong, readwrite) PXImageFetchOperationQueue *networkingQueue;
@property (nonatomic, strong, readwrite) NSOperationQueue *privateNetworkingQueue;
@property (nonatomic, strong, readwrite) NSString *currentSubreddit;
@property (nonatomic, strong) NSString *lastAfter;
@property (nonatomic, strong) NSString *lastFetchAfter;

@property (nonatomic, strong) NSString *lastCommentsAfter;
@property (nonatomic, strong) NSString *lastFetchCommentsAfter;
@property (nonatomic, strong) NSManagedObjectID *lastCommentsManagedObjectID;

@end

@implementation PXNetworkingManager

+(PXNetworkingManager *)sharedPXNetworkingManager
{
    static PXNetworkingManager *sharedPXNetworkingManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedPXNetworkingManager = [[self alloc] init];
    });
    return sharedPXNetworkingManager;
}

-(id)init
{
    self = [super init];
    if (self) {
        _networkingQueue = [PXImageFetchOperationQueue new];
		_privateNetworkingQueue = [NSOperationQueue new];
		_currentSubreddit = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentSubReddit"];
    }
    return self;
}

-(id)fetchDataForCall:(NSString *)apiURL
{
	NSString *appendSubredditString;
	if (self.currentSubreddit && ![self.currentSubreddit isEqualToString:@""]) {
		appendSubredditString = [NSString stringWithFormat:@"%@/r/%@", kBaseURL, self.currentSubreddit];
	}else{
		appendSubredditString = kBaseURL;
	}
	
    NSString *completeAPIURL = [appendSubredditString stringByAppendingString:apiURL];
    NSString *signedURL = [NSString stringWithFormat:@"%@%@",completeAPIURL, kDevelopmentKey];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:signedURL]
														   cachePolicy:NSURLRequestReloadRevalidatingCacheData
													   timeoutInterval:kDefaultTimeout];
	[request setValue:kUserAgent forHTTPHeaderField:@"User-Agent"];
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (error) {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [errorAlert show];
			
        }];
        return nil;
    }
    
    if (responseData) {
        NSJSONSerialization *jsonParser = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&error];
        return jsonParser;
    }
    
    return nil;
}

-(void)setupCurrentSubreddit:(NSString *)currentSubreddit
{
	self.currentSubreddit = currentSubreddit;
	[[NSUserDefaults standardUserDefaults] setObject:currentSubreddit forKey:@"currentSubReddit"];
}

-(void)fetchFrontScreenWithCompletion:(void(^)(void))completion
{
	//TODO: Need to switch out all these operations to be key based similar to images. 
	 // NEED TO FIND OUT HOW OFTEN THIS IS GETTING CALLED. ALSO VOODOO HAPPENS WITH OTHER THREADS IF WE BREAK HERE
	[self.privateNetworkingQueue addOperationWithBlock:^{
		[ListingObject resetAllDates];
		NSDictionary *responseData = [self fetchDataForCall:[NSString stringWithFormat:@"/.json?limit=%i&sort=confidence", kNumberToFetch]];
		NSDictionary *data = responseData[@"data"];
		self.lastAfter = [data objectForKey:@"after"];
		if (!self.lastAfter) {
			NSLog(@"ERROR");
		}
		NSArray *children = data[@"children"];
		
		NSManagedObjectContext *currentContext = [MainCoreDataManager currentContext];
		for (int i = 0; i < children.count ; i++) {
			NSDictionary *data = children[i];
			NSDictionary *child = data[@"data"];
			[ListingObject parseListingObjectForDictionary:child withOrder:i withAddedTime:[NSDate date] onContext:currentContext];
		}
		[currentContext save:nil];
		[[NSOperationQueue mainQueue] addOperationWithBlock:^{
			if (completion)
				completion();
		}];
	}];
}

-(void)fetchMore
{
	if ([self.lastFetchAfter isEqualToString:self.lastAfter]) {
		return;
	}
	[self fetchMoreAfter:self.lastAfter];
}

-(void)fetchMoreAfter:(NSString *)after
{
	self.lastFetchAfter = after;
	[self.privateNetworkingQueue addOperationWithBlock:^{
		NSString *fetchCall = [NSString stringWithFormat:@"/.json?limit=%i&sort=confidence&after=%@", kNumberToFetch, after];
		NSDictionary *responseData = [self fetchDataForCall:fetchCall];
		NSDictionary *data = responseData[@"data"];
		self.lastAfter = [data objectForKey:@"after"];
		if (!self.lastAfter) {
			NSLog(@"ERROR");
		}
		NSManagedObjectContext *currentContext = [MainCoreDataManager currentContext];
		NSArray *children = data[@"children"];
		for (int i = 0; i < children.count ; i++) {
			NSDictionary *data = children[i];
			NSDictionary *child = data[@"data"];
			[ListingObject parseListingObjectForDictionary:child withOrder:i withAddedTime:[NSDate date] onContext:currentContext];
		}
		[currentContext save:nil];
	}];
}

-(void)fetchImageForKey:(NSString *)key
{
	PXImageFetchOperation *fetchOperation = [[PXImageFetchOperation alloc] initWithKey:key];
	[self.networkingQueue addOperation:fetchOperation];
}

-(void)cancelImageForKey:(NSString *)key
{
	[self.networkingQueue cancelOperationWithKey:key]; 
}

-(void)loginWithUsername:(NSString *)user andPassword:(NSString *)pass
{
	[self.privateNetworkingQueue addOperationWithBlock:^{
		NSString *completeAPIURL = [kBaseURL stringByAppendingString:@"/api/login"];
		NSString *signedURL = [NSString stringWithFormat:@"%@%@",completeAPIURL, kDevelopmentKey];
		NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:signedURL]
															   cachePolicy:NSURLRequestReloadRevalidatingCacheData
														   timeoutInterval:kDefaultTimeout];
		[request setValue:kUserAgent forHTTPHeaderField:@"User-Agent"];
		NSString *bodyString = [NSString stringWithFormat:@"user=%@&passwd=%@&api_type=json&rem=False",user,pass];
		[request setHTTPMethod:@"POST"];
		[request setHTTPBody:[bodyString dataUsingEncoding:NSUTF8StringEncoding]];
		
		NSHTTPURLResponse *response = nil;
		NSError *error = nil;
		NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
		if (error) {
			[[NSOperationQueue mainQueue] addOperationWithBlock:^{
				UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
				[errorAlert show];
				
			}];
			
		}
		
		if (responseData) {
			NSJSONSerialization *jsonParser = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&error];
			NSLog(@"Response %@ %@", [response allHeaderFields], jsonParser);
		}
	}];
}

-(void)fetchCommentsForListingObject:(RedditObject *)redditObject
{
//http://api.reddit.com/r/pics/comments/253dr5/the_most_badass_helmet_ever_crafted/?sort=confidence
	//need to use the placeholder above (http://api.reddit.com/PLACEHOLDER)
	//nb: there are some comments that say more, if user accesses this low we should pull more using the method below.
	NSManagedObjectID *managedObjectID = redditObject.objectID;
	[self.privateNetworkingQueue addOperationWithBlock:^{
		NSManagedObjectContext *currentcontext = [MainCoreDataManager currentContext];
		RedditObject *newRedditObject = (RedditObject *)[currentcontext objectWithID:managedObjectID];
		self.lastCommentsManagedObjectID = redditObject.objectID;
		NSString *commentsURL = [NSString stringWithFormat:@"%@%@?&sort=best", kBaseURL, newRedditObject.permalink];

		NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:commentsURL]
															   cachePolicy:NSURLRequestReloadRevalidatingCacheData
														   timeoutInterval:kDefaultTimeout];
		[request setValue:kUserAgent forHTTPHeaderField:@"User-Agent"];
		[request setHTTPMethod:@"GET"];
		
		NSHTTPURLResponse *response = nil;
		NSError *error = nil;
		NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
		if (error) {
			[[NSOperationQueue mainQueue] addOperationWithBlock:^{
				UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
				[errorAlert show];
				
			}];
			
		}
		
		if (responseData) {
			//2nd item in array
			NSJSONSerialization *jsonParser = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&error];
			NSLog(@"Response %@", [response allHeaderFields]);
			
			NSArray *jsonReturnArray = (NSArray *)jsonParser;
			NSDictionary *returnType = jsonReturnArray[1];
			NSArray *newType = returnType[@"data"][@"children"]; //all comments
			NSLog(@"return %@", newType);
			self.lastCommentsAfter = returnType[@"data"][@"after"];
			for (int i = 0; i < [newType count]; i++) {
				NSDictionary *dic = newType[i];
				NSDictionary *parseDic = dic[@"data"];
				Comment *comment = [Comment parseCommentForDictionary:parseDic withOrder:i withAddedTime:[NSDate date] withContext:currentcontext];
				if ([newRedditObject isKindOfClass:[ListingObject class]]) {
					[(ListingObject *)newRedditObject addCommentsObject:comment];
				}
			}
			[currentcontext save:nil];
		}
	}];
}

-(void)fetchMoreCommentsAfter:(NSString *)after withManagedObjectID:(NSManagedObjectID *)managedObjectID
{
	self.lastFetchCommentsAfter = after;
	[self.privateNetworkingQueue addOperationWithBlock:^{
		NSManagedObjectContext *currentContext = [MainCoreDataManager currentContext];
		RedditObject *newRedditObject = (RedditObject *)[currentContext objectWithID:managedObjectID];
		NSString *fetchCall = [NSString stringWithFormat:@"http://www.reddit.com/r/%@/comments/%@/.json?limit=%i&after=%@", newRedditObject.subreddit, newRedditObject.obj_id, kNumberToFetch, after];
		NSDictionary *responseData = [self fetchDataForCall:fetchCall];
		NSDictionary *data = responseData[@"data"];
		self.lastCommentsAfter = [data objectForKey:@"after"];
		if (!self.lastCommentsAfter) {
			NSLog(@"ERROR");
		}
		
		NSArray *children = data[@"children"];
		for (int i = 0; i < children.count ; i++) {
			NSDictionary *data = children[i];
			NSDictionary *child = data[@"data"];
			[ListingObject parseListingObjectForDictionary:child withOrder:i withAddedTime:[NSDate date] onContext:currentContext];
		}
		[currentContext save:nil];
	}];
}

-(void)fetchMoreComments
{
	if (self.lastFetchCommentsAfter) {
		[self fetchMoreCommentsAfter:self.lastCommentsAfter withManagedObjectID:self.lastCommentsManagedObjectID];
	}
}


#pragma mark - IMGUR

-(NSDictionary *)fetchImgurDataForImgurID:(NSString *)imgurID
{
	NSString *imgurURL = @"https://api.imgur.com/3/image/";
	
    NSString *completeAPIURL = [imgurURL stringByAppendingString:imgurID];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:completeAPIURL]
														   cachePolicy:NSURLRequestReloadRevalidatingCacheData
													   timeoutInterval:kDefaultTimeout];
	
	[request addValue:@"Client-ID 4c72988a43c23b3" forHTTPHeaderField:@"Authorization"];
	[request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	
	NSHTTPURLResponse *response = nil;
	NSError *error = nil;
	NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
	if (error) {
		[[NSOperationQueue mainQueue] addOperationWithBlock:^{
			UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[errorAlert show];
			
		}];
		
	}
	NSJSONSerialization *jsonParser = nil;
	if (responseData) {
		jsonParser = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&error];
		NSLog(@"Response %@ %@", [response allHeaderFields], jsonParser);
	}
	return (NSDictionary *)jsonParser;
}

@end
