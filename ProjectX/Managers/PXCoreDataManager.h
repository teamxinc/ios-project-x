//
//  PXCoreDataManager.h
//  ProjectX
//
//  Created by David Attaie on 26/01/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

#define MainCoreDataManager [PXCoreDataManager sharedPXCoreDataManager]

@interface PXCoreDataManager : NSObject

+ (PXCoreDataManager *)sharedPXCoreDataManager;
- (NSManagedObjectContext *)currentContext;
- (NSManagedObjectContext *)mainContext;
- (void)saveContext;
- (void)saveBackgroundContext;
- (NSURL *)applicationDocumentsDirectory;

-(id)managedObjectForEntity:(NSString *)entityString
		withPredicateString:(NSPredicate *)predicate
  usingManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;

@end