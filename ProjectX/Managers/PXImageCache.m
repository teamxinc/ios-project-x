//
//  PXImageCache.m
//  ProjectX
//
//  Created by David Attaie on 06/02/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "PXImageCache.h"

@interface PXImageCache()

@property (nonatomic, strong) NSMutableArray *keysAdded;

@end

@implementation PXImageCache

+(PXImageCache *)sharedPXImageCache
{
    static PXImageCache *sharedPXImageCache = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedPXImageCache = [[self alloc] init];
    });
    return sharedPXImageCache;
}

-(id)init
{
	self = [super init];
	if (self) {
		_keysAdded = [NSMutableArray new];
	}
	return self;
}

-(UIImage *)imageForKey:(NSString *)key
{
	NSData *imageData = [self objectForKey:key];
	UIImage *image = [UIImage imageWithData:imageData];
	return image;
}

-(void)setObject:(NSObject *)object forKey:(id<NSCopying>)aKey
{
	[[NSOperationQueue mainQueue] addOperationWithBlock:^{
		[super setObject:object forKey:aKey];
		[self.keysAdded addObject:aKey];
		[[NSNotificationCenter defaultCenter] postNotificationName:kImageDownloadedNotification object:nil userInfo:@{@"imagekey": aKey}];
		NSLog(@"Downloaded image for key |%@|", aKey);
	}];
}

@end
