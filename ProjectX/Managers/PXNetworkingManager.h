//
//  PXNetworkingManager.h
//  ProjectX
//
//  Created by David Attaie on 26/01/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PXImageFetchOperationQueue.h"

#define MainNetworkingManager [PXNetworkingManager sharedPXNetworkingManager]

@class RedditObject;

@interface PXNetworkingManager : NSObject

@property (nonatomic, strong, readonly) PXImageFetchOperationQueue *networkingQueue;
@property (nonatomic, strong, readonly) NSString *currentSubreddit;

+ (PXNetworkingManager *)sharedPXNetworkingManager;

-(void)fetchFrontScreenWithCompletion:(void(^)(void))completion;
-(void)setupCurrentSubreddit:(NSString *)currentSubreddit;
-(void)fetchMore;

-(void)fetchImageForKey:(NSString *)key;
-(void)cancelImageForKey:(NSString *)key;

//Imgur
-(NSDictionary *)fetchImgurDataForImgurID:(NSString *)imgurID;

-(void)loginWithUsername:(NSString *)user andPassword:(NSString *)pass;

-(void)fetchCommentsForListingObject:(RedditObject *)redditObject;
-(void)fetchMoreComments;
@end
