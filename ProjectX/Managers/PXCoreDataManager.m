//
//  PXCoreDataManager.m
//  ProjectX
//
//  Created by David Attaie on 26/01/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "PXCoreDataManager.h"

@interface PXCoreDataManager()

@property (readwrite, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readwrite, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readwrite, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readwrite, strong, nonatomic) NSManagedObjectContext *multiThreadedObjectContext;
@property (nonatomic, strong, readwrite) NSOperationQueue *coreDataBackgroundQueue;

@end

@implementation PXCoreDataManager

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;


-(id)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(managedObjectContextChanged:) name:NSManagedObjectContextDidSaveNotification object:nil];
		_coreDataBackgroundQueue = [NSOperationQueue new];
    }
    return self;
}

+(PXCoreDataManager *)sharedPXCoreDataManager
{
    static PXCoreDataManager *sharedPXCoreDataManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedPXCoreDataManager = [[self alloc] init];
    });
    return sharedPXCoreDataManager;
}

-(NSManagedObjectContext *)currentContext
{
	NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
	[context setParentContext:self.managedObjectContext];
	return context;
}

//MAIN THREAD CONTEXT BEWARE OF MULTITHREADING AND BLOCKING
-(NSManagedObjectContext *)mainContext
{
	return self.managedObjectContext;
}

-(void)managedObjectContextChanged:(NSNotification *)notification
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        NSManagedObjectContext *incomingContext = [notification object];
        NSPersistentStoreCoordinator *incomingCoordinator = [incomingContext persistentStoreCoordinator];
        
        if (incomingCoordinator != self.persistentStoreCoordinator) {
            return;
        }
        [self.managedObjectContext mergeChangesFromContextDidSaveNotification:notification];
        [self saveContext];
    }];
}

- (void)saveContext
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        NSError *error = nil;
        NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
        
        if (managedObjectContext != nil) {
            
            if ([managedObjectContext hasChanges]) {
                if (![managedObjectContext save:&error]) {
                    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                    abort();
                }else{
                    NSLog(@"Save Success");
                }
            }
        }
    }];
}

-(void)saveBackgroundContext
{
	[self.coreDataBackgroundQueue addOperationWithBlock:^{
        NSError *error = nil;
        NSManagedObjectContext *managedObjectContext = self.multiThreadedObjectContext;
        
        if (managedObjectContext != nil) {
            
            if ([managedObjectContext hasChanges]) {
                if (![managedObjectContext save:&error]) {
                    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                    abort();
                }else{
                    NSLog(@"Save Success");
                }
            }
        }
    }];
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        NSUInteger type = NSMainQueueConcurrencyType;
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:type];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"ProbeModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ProbeModel.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

///check if the object exists, if so return the managed object. If not then return a new instance.
-(id)managedObjectForEntity:(NSString *)entityString
		withPredicateString:(NSPredicate *)predicate
  usingManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
	//we try to fetch the object incase it already exists
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *description = [NSEntityDescription entityForName:entityString inManagedObjectContext:managedObjectContext];
	[request setEntity:description];
	
	[request setPredicate:predicate];
	[managedObjectContext lock];
	NSArray *result = [managedObjectContext executeFetchRequest:request error:nil];
	[managedObjectContext unlock];
	
	if (result.count > 0) {
		return [result firstObject];
	}
	
	return [NSEntityDescription insertNewObjectForEntityForName:entityString
										 inManagedObjectContext:managedObjectContext];
}

@end