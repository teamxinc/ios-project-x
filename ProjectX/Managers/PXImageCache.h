//
//  PXImageCache.h
//  ProjectX
//
//  Created by David Attaie on 06/02/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PXImageCache : NSCache

+(PXImageCache *)sharedPXImageCache;
-(UIImage *)imageForKey:(NSString *)key;

@end
