//
//  PXModelCache.m
//  ProjectX
//
//  Created by David Attaie on 06/02/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "PXModelCache.h"

@implementation PXModelCache

+(PXModelCache *)sharedPXModelCache
{
    static PXModelCache *sharedPXModelCache = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedPXModelCache = [[self alloc] init];
    });
    return sharedPXModelCache;
}

-(id)init{
	self = [super init];
	if (self) {
		_arrayOfCurrentItems = [NSMutableArray new];
	}
	return self;
}

-(void)setObject:(id)obj forKey:(id)key
{
	[super setObject:obj forKey:key];
	[self.arrayOfCurrentItems addObject:key];
}

@end
