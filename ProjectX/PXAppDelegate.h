//
//  PXAppDelegate.h
//  ProjectX
//
//  Created by David Attaie on 26/01/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PXAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end
