//
//  main.m
//  ProjectX
//
//  Created by David Attaie on 26/01/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PXAppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([PXAppDelegate class]));
	}
}
