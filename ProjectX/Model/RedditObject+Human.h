//
//  RedditObject+Human.h
//  Probe
//
//  Created by David Attaie on 03/04/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "RedditObject.h"

@interface RedditObject (Human)

+(RedditObject *)parseRedditObjectForDictionary:(NSDictionary *)dictionary withContext:(NSManagedObjectContext *)context;
+(RedditObject *)setupRedditObject:(RedditObject *)object withDictionary:(NSDictionary *)dictionary;

@end
