//
//  PXListingObject.m
//  ProjectX
//
//  Created by David Attaie on 06/02/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "PXNetworkingManager.h"
#import "PXListingObject.h"
#import "PXImageCache.h"

@implementation PXListingObject

-(void)setupWithData:(NSDictionary *)data
{
	self.score = [self validateDataInDictionary:data forKey:@"score"];
	self.created_utc = [self validateDataInDictionary:data forKey:@"created_utc"];
	self.clicked = [self validateDataInDictionary:data forKey:@"clicked"];
	self.visited = [self validateDataInDictionary:data forKey:@"visited"];
	self.listing_id = [self validateDataInDictionary:data forKey:@"id"];
	self.author = [self validateDataInDictionary:data forKey:@"author"];
	self.title = [self validateDataInDictionary:data forKey:@"title"];
	self.over_18 = [self validateDataInDictionary:data forKey:@"over_18"];
	self.created = [self validateDataInDictionary:data forKey:@"created"];
	self.name = [self validateDataInDictionary:data forKey:@"name"];
	self.domain = [self validateDataInDictionary:data forKey:@"domain"];
	self.subreddit = [self validateDataInDictionary:data forKey:@"subreddit"];
	self.main_text = [self validateDataInDictionary:data forKey:@"selftext_html"];
	self.plain_text = [self validateDataInDictionary:data forKey:@"selftext"];
	self.url = [self validateDataInDictionary:data forKey:@"url"];
	if ([self.domain isEqualToString:@"imgur.com"] ||
		[self.domain isEqualToString:@"i.imgur.com"]) {
		NSString *last3 = [self.url substringFromIndex:self.url.length - 3];
		if ([last3 isEqualToString:@"jpg"] ||
			[last3 isEqualToString:@"gif"] ||
			[last3 isEqualToString:@"png"] ||
			[last3 isEqualToString:@"jpeg"]) {
		}else{
			self.url = [self.url stringByAppendingString:@".jpg"];
		}
		[self fetchImageForKey:self.url];
		if ([last3 isEqualToString:@"gif"]) {
//			[self setListingType:ListingObjectTypeGif];
		}else{
			[self setListingType:ListingObjectTypePhoto];
		}
	}
	if (!self.listingType) {
		self.listingType = ListingObjectTypeText;
	}
}

-(id)validateDataInDictionary:(NSDictionary *)data forKey:(NSString *)key
{
	if (![data[key] isKindOfClass:[NSNull class]]) {
		return data[key];
	}
	return nil;
}

-(void)fetchImageForKey:(NSString *)key
{
	[[PXNetworkingManager sharedPXNetworkingManager].networkingQueue addOperationWithBlock:^{

			if ([[PXImageCache sharedPXImageCache] objectForKey:key]) {
				return;
			}
			NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:key]];
			//		UIImage *image = [UIImage imageWithData:imageData];
			if (imageData) {
				[[PXImageCache sharedPXImageCache] setObject:imageData forKey:key];
				[[NSOperationQueue mainQueue] addOperationWithBlock:^{
					self.image = [UIImage imageWithData:imageData];
				}];
			}else{
				NSLog(@"ERROR Saving Image");
			}
//			[[NSNotificationCenter defaultCenter] postNotificationName:@"ImageUpdate" object:nil];

	}];
}


@end
