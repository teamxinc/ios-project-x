//
//  ListingObject+Human.h
//  Probe
//
//  Created by David Attaie on 03/04/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "ListingObject.h"

@interface ListingObject (Human)

+(ListingObject *)parseListingObjectForDictionary:(NSDictionary *)dictionary withOrder:(int)order withAddedTime:(NSDate *)dateAdded onContext:(NSManagedObjectContext *)context;
+(NSArray *)getAll;
+(void)resetAllDates;
-(void)setupListingType;
//-(void)fetchImage;

@end
