//
//  RedditObject+Human.m
//  Probe
//
//  Created by David Attaie on 03/04/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "RedditObject+Human.h"
#import "PXCoreDataManager.h"
#import "PXProbeHelpers.h"

@implementation RedditObject (Human)

+(RedditObject *)parseRedditObjectForDictionary:(NSDictionary *)dictionary withContext:(NSManagedObjectContext *)context
{
	RedditObject *object = [MainCoreDataManager managedObjectForEntity:@"RedditObject"
												   withPredicateString:[NSPredicate predicateWithFormat:@"obj_id == %@", dictionary[@"id"]]
											 usingManagedObjectContext:context];
	[RedditObject setupRedditObject:object withDictionary:dictionary];
	
	return object;
}

+(RedditObject *)setupRedditObject:(RedditObject *)object withDictionary:(NSDictionary *)dictionary
{
	object.author = [PXProbeHelpers validateDataInDictionary:dictionary forKey:@"author"];
	object.date_added = [PXProbeHelpers validateDataInDictionary:dictionary forKey:@"dateadded"];
	object.order_added = [PXProbeHelpers validateDataInDictionary:dictionary forKey:@"orderadded"];
	object.obj_id = [PXProbeHelpers validateDataInDictionary:dictionary forKey:@"id"];
	object.clicked = [PXProbeHelpers validateDataInDictionary:dictionary forKey:@"clicked"];
	object.author = [PXProbeHelpers validateDataInDictionary:dictionary forKey:@"author"];
	object.downs = [PXProbeHelpers validateDataInDictionary:dictionary forKey:@"downs"];
	object.ups = [PXProbeHelpers validateDataInDictionary:dictionary forKey:@"ups"];
	object.edited = [PXProbeHelpers validateDataInDictionary:dictionary forKey:@"edited"];
	object.text = [PXProbeHelpers validateDataInDictionary:dictionary forKey:@"selftext"];
	object.text_html = [PXProbeHelpers validateDataInDictionary:dictionary forKey:@"selftext_html"];
	object.url = [PXProbeHelpers validateDataInDictionary:dictionary forKey:@"url"];
	object.title = [PXProbeHelpers validateDataInDictionary:dictionary forKey:@"title"];
	object.subreddit = [PXProbeHelpers validateDataInDictionary:dictionary forKey:@"subreddit"];
	object.permalink = [PXProbeHelpers validateDataInDictionary:dictionary forKey:@"permalink"];
	NSNumber *dateValue = [PXProbeHelpers validateDataInDictionary:dictionary forKey:@"created_utc"];
	
	if (dateValue) {
		object.created_utc = [NSDate dateWithTimeIntervalSince1970:[dateValue doubleValue]];
	}
	return object;
}

@end
