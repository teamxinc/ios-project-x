//
//  ListingObjectGalleryItem.h
//  Probe
//
//  Created by David Attaie on 22/06/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ListingObject;

@interface ListingObjectGalleryItem : NSManagedObject

@property (nonatomic, retain) NSString * urlofimage;
@property (nonatomic, retain) NSString * imageMetaData;
@property (nonatomic, retain) ListingObject *parentObject;

@end
