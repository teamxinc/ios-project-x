//
//  ListingObjectGalleryItem+Human.h
//  Probe
//
//  Created by David Attaie on 22/06/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "ListingObjectGalleryItem.h"

@interface ListingObjectGalleryItem (Human)

+(ListingObjectGalleryItem *)galleryItemWithURL:(NSString *)url onContext:(NSManagedObjectContext *)context;

@end
