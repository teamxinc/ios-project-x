//
//  ListingObject.h
//  Probe
//
//  Created by David Attaie on 22/06/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "RedditObject.h"

@class Comment;

@interface ListingObject : RedditObject

@property (nonatomic, retain) NSNumber * created;
@property (nonatomic, retain) NSString * domain;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSNumber * imageHeight;
@property (nonatomic, retain) NSNumber * imageWidth;
@property (nonatomic, retain) NSNumber * numComments;
@property (nonatomic, retain) NSNumber * over_18;
@property (nonatomic, retain) NSNumber * visited;
@property (nonatomic, retain) NSSet *comments;
@property (nonatomic, retain) NSSet *galleryItems;
@end

@interface ListingObject (CoreDataGeneratedAccessors)

- (void)addCommentsObject:(Comment *)value;
- (void)removeCommentsObject:(Comment *)value;
- (void)addComments:(NSSet *)values;
- (void)removeComments:(NSSet *)values;

- (void)addGalleryItemsObject:(NSManagedObject *)value;
- (void)removeGalleryItemsObject:(NSManagedObject *)value;
- (void)addGalleryItems:(NSSet *)values;
- (void)removeGalleryItems:(NSSet *)values;

@end
