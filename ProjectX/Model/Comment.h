//
//  Comment.h
//  Probe
//
//  Created by David Attaie on 18/06/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "RedditObject.h"

@class Comment, ListingObject;

@interface Comment : RedditObject

@property (nonatomic, retain) NSNumber * expanded;
@property (nonatomic, retain) NSNumber * commentDepth;
@property (nonatomic, retain) NSSet *children;
@property (nonatomic, retain) Comment *parentComment;
@property (nonatomic, retain) ListingObject *parentListingObject;
@end

@interface Comment (CoreDataGeneratedAccessors)

- (void)addChildrenObject:(Comment *)value;
- (void)removeChildrenObject:(Comment *)value;
- (void)addChildren:(NSSet *)values;
- (void)removeChildren:(NSSet *)values;

@end
