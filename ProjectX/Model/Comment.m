//
//  Comment.m
//  Probe
//
//  Created by David Attaie on 18/06/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "Comment.h"
#import "Comment.h"
#import "ListingObject.h"


@implementation Comment

@dynamic expanded;
@dynamic commentDepth;
@dynamic children;
@dynamic parentComment;
@dynamic parentListingObject;

@end
