//
//  ListingObject+Human.m
//  Probe
//
//  Created by David Attaie on 03/04/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "NSString+Substrings.h"
#import "ListingObject+Human.h"
#import "PXNetworkingManager.h"
#import "RedditObject+Human.h"
#import "PXCoreDataManager.h"
#import "PXProbeHelpers.h"
#import "PXImageCache.h"

@interface ListingObject()

@end

@implementation ListingObject (Human)

+(ListingObject *)parseListingObjectForDictionary:(NSDictionary *)dictionary withOrder:(int)order withAddedTime:(NSDate *)dateAdded onContext:(NSManagedObjectContext *)context
{
	ListingObject *object = [MainCoreDataManager managedObjectForEntity:@"ListingObject"
													withPredicateString:[NSPredicate predicateWithFormat:@"obj_id == %@", dictionary[@"id"]]
											  usingManagedObjectContext:context];
	
	NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithDictionary:dictionary];
	[mutableDic setObject:dateAdded forKey:@"dateadded"];
	[mutableDic setObject:[NSNumber numberWithInt:order] forKey:@"orderadded"];
	[RedditObject setupRedditObject:object withDictionary:[mutableDic copy]];
	object.domain = [PXProbeHelpers validateDataInDictionary:dictionary forKey:@"domain"];
	object.url = [PXProbeHelpers validateDataInDictionary:dictionary forKey:@"url"];
	object.permalink = [PXProbeHelpers validateDataInDictionary:dictionary forKey:@"permalink"];
	object.numComments = [PXProbeHelpers validateDataInDictionary:dictionary forKey:@"num_comments"];
	//image is the key for the image
	[object setupListingType];
	
	return object;
}

+(NSArray *)getAll
{
	return [self getAllWithContext:[MainCoreDataManager currentContext]];
}

+(NSArray *)getAllWithContext:(NSManagedObjectContext *)context
{
	NSManagedObjectContext *updateContext = context;
	NSFetchRequest *request = [[NSFetchRequest alloc] init];
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"ListingObject" inManagedObjectContext: updateContext];
	[request setEntity:entity];
	
	NSError *error;
	return [updateContext executeFetchRequest:request error:&error];
}

+(void)resetAllDates
{
	NSManagedObjectContext *context = [MainCoreDataManager currentContext];
	NSArray *allObjects = [ListingObject getAllWithContext:context];
	for (ListingObject *object in allObjects) {
		[object setDate_added:nil];
	}
	NSError *save = nil;
	[context save:&save];
	if (save.localizedDescription) {
		NSLog(@"ERROR %@", save.localizedDescription);
	}
//	[[NSNotificationCenter defaultCenter] postNotificationName:kViewUpdateNotification object:nil];
}

-(void)setupListingType
{
	//domain i.imgur.com is single image, imgur.com is library of images ugh.
	if ([self.domain isEqualToString:@"i.imgur.com"]) {
		
		self.imageType = [NSNumber numberWithInt:ListingObjectTypePhoto];
		
		NSArray *componentsArray = [self.url pathComponents];
		NSString *lastItemInArray = [componentsArray lastObject];
		
		if ([lastItemInArray rangeOfString:@"."].location != NSNotFound) {
			lastItemInArray = [lastItemInArray substringToIndex:[lastItemInArray rangeOfString:@"."].location];
		}
		
		NSDictionary *imageType = [MainNetworkingManager fetchImgurDataForImgurID:lastItemInArray];
		NSString *mimeType = imageType[@"data"][@"type"];
		
		self.imageHeight = imageType[@"data"][@"height"];
		self.imageWidth = imageType[@"data"][@"width"];
		self.url = imageType[@"data"][@"link"];
		
		if ([mimeType containsSubstring:@"jpg"] || [mimeType containsSubstring:@"jpeg"] || [mimeType containsSubstring:@"png"]) {
			self.imageType = [NSNumber numberWithInt:ListingObjectTypePhoto];
		}else if ([mimeType containsSubstring:@"gif"])
		{
			self.imageType = [NSNumber numberWithInt:ListingObjectTypeGif];
		}
		
		[MainNetworkingManager fetchImageForKey:self.url];

	}else if([self.domain isEqualToString:@"imgur.com"]){ //gallery
		self.imageType = [NSNumber numberWithInt:ListingObjectTypeWebPage];
		
	}else if ([self.domain containsSubstring:@"self"]){
		self.imageType = [NSNumber numberWithInt:ListingObjectTypeText];
	}else if (self.url) {
		self.imageType = [NSNumber numberWithInt:ListingObjectTypeWebPage];
	}else{
		self.imageType = [NSNumber numberWithInt:ListingObjectTypeText];
	}
}

//-(void)fetchImage
//{
//	if (self.url)
//		[self fetchImageForKey:self.url];
//}
//
//-(void)fetchImageForKey:(NSString *)key
//{
//	[[PXNetworkingManager sharedPXNetworkingManager].networkingQueue addOperationWithBlock:^{
//		if ([[PXImageCache sharedPXImageCache] objectForKey:key]) {
//			return;
//		}
//		
//		NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:key]];
//		if (imageData) {
//			[[PXImageCache sharedPXImageCache] setObject:imageData forKey:key]; //save the image to cache
//		}else{
//			NSLog(@"ERROR Saving Image with URL %@", key);
//		}
//	}];
//}

@end