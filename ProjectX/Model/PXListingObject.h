//
//  PXListingObject.h
//  ProjectX
//
//  Created by David Attaie on 06/02/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PXStaticHeader.h"

@interface PXListingObject : NSObject

@property (nonatomic, retain) NSNumber * created_utc;
@property (nonatomic, retain) NSNumber * score;
@property (nonatomic, retain) NSNumber * clicked;
@property (nonatomic, retain) NSNumber * visited;
@property (nonatomic, retain) NSString * listing_id;
@property (nonatomic, retain) NSString * author;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * over_18;
@property (nonatomic, retain) NSNumber * created;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * main_text;
@property (nonatomic, retain) NSString * domain;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * subreddit;
@property (nonatomic, retain) NSString * plain_text;
@property (nonatomic, retain) NSString * urlOfImage;
@property (nonatomic, retain) UIImage * image;
@property (nonatomic) ListingObjectType listingType;

-(void)setupWithData:(NSDictionary *)data;

@end
