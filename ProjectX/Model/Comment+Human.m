//
//  Comment+Human.m
//  Probe
//
//  Created by David Attaie on 03/04/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "RedditObject+Human.h"
#import "PXCoreDataManager.h"
#import "PXProbeHelpers.h"
#import "Comment+Human.h"

@implementation Comment (Human)

+(Comment *)parseCommentForDictionary:(NSDictionary *)dictionary withOrder:(int)order withAddedTime:(NSDate *)dateAdded withContext:(NSManagedObjectContext *)context
{
	Comment *object = [MainCoreDataManager managedObjectForEntity:@"Comment"
											  withPredicateString:[NSPredicate predicateWithFormat:@"obj_id == %@", dictionary[@"id"]]
										usingManagedObjectContext:context];
	
	NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithDictionary:dictionary];
	[mutableDic setObject:dateAdded forKey:@"dateadded"];
	[mutableDic setObject:[NSNumber numberWithInt:order] forKey:@"orderadded"];
	
	[RedditObject setupRedditObject:object withDictionary:mutableDic];
	object.text = [PXProbeHelpers validateDataInDictionary:mutableDic forKey:@"body"];
	
	id replies = [PXProbeHelpers validateDataInDictionary:mutableDic forKey:@"replies"];
	if ([replies isKindOfClass:[NSDictionary class]]) {
		NSDictionary *data = replies[@"data"];
		if (data) {
			NSArray *children = data[@"children"];
			if (children.count > 0) {
				for (int i = 0; i < [children count]; i++) {
					id childObject = children[i];
					NSDictionary *childData = childObject[@"data"];
					Comment *child = [Comment parseCommentForDictionary:childData withOrder:i withAddedTime:dateAdded withContext:context];
					[child setParentComment:object];
					[child setCommentDepth: [NSNumber numberWithInt:[object.commentDepth intValue] + 1]];
					//TODO: if its a child as a string, then we have to fetch that child and its subsequent children if the user asks for more... do it later.
				}
			}
		}
	}

	return object;
}

@end
