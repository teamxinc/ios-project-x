//
//  ListingObjectGalleryItem.m
//  Probe
//
//  Created by David Attaie on 22/06/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "ListingObjectGalleryItem.h"
#import "ListingObject.h"


@implementation ListingObjectGalleryItem

@dynamic urlofimage;
@dynamic imageMetaData;
@dynamic parentObject;

@end
