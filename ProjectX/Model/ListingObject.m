//
//  ListingObject.m
//  Probe
//
//  Created by David Attaie on 22/06/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "ListingObject.h"
#import "Comment.h"


@implementation ListingObject

@dynamic created;
@dynamic domain;
@dynamic image;
@dynamic imageHeight;
@dynamic imageWidth;
@dynamic numComments;
@dynamic over_18;
@dynamic visited;
@dynamic comments;
@dynamic galleryItems;

@end
