//
//  ListingObjectGalleryItem+Human.m
//  Probe
//
//  Created by David Attaie on 22/06/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "ListingObjectGalleryItem+Human.h"
#import "PXCoreDataManager.h"
#import "PXProbeHelpers.h"
#import "PXImageCache.h"

@implementation ListingObjectGalleryItem (Human)

+(ListingObjectGalleryItem *)galleryItemWithURL:(NSString *)url onContext:(NSManagedObjectContext *)context
{
	ListingObjectGalleryItem *object = [MainCoreDataManager managedObjectForEntity:@"ListingObjectGalleryItem"
															   withPredicateString:[NSPredicate predicateWithFormat:@"urlofimage == %@", url]
														 usingManagedObjectContext:context];
	object.urlofimage = url;
	return object;
}

@end
