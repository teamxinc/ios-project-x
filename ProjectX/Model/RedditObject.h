//
//  RedditObject.h
//  Probe
//
//  Created by David Attaie on 12/06/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RedditObject;

@interface RedditObject : NSManagedObject

@property (nonatomic, retain) NSString * author;
@property (nonatomic, retain) NSNumber * clicked;
@property (nonatomic, retain) NSDate * created_utc;
@property (nonatomic, retain) NSDate * date_added;
@property (nonatomic, retain) NSNumber * downs;
@property (nonatomic, retain) NSNumber * edited;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * obj_id;
@property (nonatomic, retain) NSNumber * order_added;
@property (nonatomic, retain) NSString * permalink;
@property (nonatomic, retain) NSNumber * score_hidden;
@property (nonatomic, retain) NSString * subreddit;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSString * text_html;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * ups;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSNumber * imageType;
@property (nonatomic, retain) RedditObject *parent;

@end
