//
//  RedditObject.m
//  Probe
//
//  Created by David Attaie on 12/06/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "RedditObject.h"
#import "RedditObject.h"


@implementation RedditObject

@dynamic author;
@dynamic clicked;
@dynamic created_utc;
@dynamic date_added;
@dynamic downs;
@dynamic edited;
@dynamic name;
@dynamic obj_id;
@dynamic order_added;
@dynamic permalink;
@dynamic score_hidden;
@dynamic subreddit;
@dynamic text;
@dynamic text_html;
@dynamic title;
@dynamic ups;
@dynamic url;
@dynamic imageType;
@dynamic parent;

@end
