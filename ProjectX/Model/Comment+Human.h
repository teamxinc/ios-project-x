//
//  Comment+Human.h
//  Probe
//
//  Created by David Attaie on 03/04/2014.
//  Copyright (c) 2014 Angry Rocket Studios. All rights reserved.
//

#import "Comment.h"

@interface Comment (Human)

+(Comment *)parseCommentForDictionary:(NSDictionary *)dictionary withOrder:(int)order withAddedTime:(NSDate *)dateAdded withContext:(NSManagedObjectContext *)context;

@end
